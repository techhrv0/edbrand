<?php

/**
 *
 * @author watch
 *        
 */
class JobQueue
{

    public function perform()
    {
        $data = $this->args;
        $logFile = __DIR__ . "/logs/job_log.log";
        
        if ($this->args['time'] <= time()) {
            $file = fopen($logFile, "a+");
            $logMsg = "";
            try {
                $ch = curl_init();
                $options = array(
                    CURLOPT_URL => $data['url'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLINFO_HEADER_OUT => true, // Request header
                    CURLOPT_HEADER => false, // Return header
                    CURLOPT_SSL_VERIFYPEER => false, // Don't veryify server certificate
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $data
                );
                curl_setopt_array($ch, $options);
                $response = curl_exec($ch);
                $logMsg = "[ " . date('Y-m-d H:i:s', time()) . " ] : " . $response . " \n";
                fwrite($file, $logMsg);
            } catch (\Exception $e) {
                $response = array(
                    'status' => 500,
                    'Message' => 'Error Occured while hit server.'
                );
                
                $logMsg = "[ " . date('Y-m-d H:i:s', time()) . " ] : " . json_encode($response) . " \n";
                fwrite($file, $logMsg);
            }
            fclose($file);
        } else {
            \Resque::setBackend($data['redisHost']);
            $jobId = \Resque::enqueue($data['queue'], $data['jobClass'], $data);
        }
    }
}

?>