var drooly = {
      getUserList: function(filter){
        var formData = {};
        formData['search'] = $('#search').val();
        formData['filter'] = filter;
        formData['pera'] = $('#pera').val();
        formData['page'] = 1;
        $.ajax({
            type: "POST",
            url: '/eventprogram/manage-user-data',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultData').html("");
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
   
    
    
    
  manageVideos: function(filter,page){
        var formData = {};
        formData['search'] = $('#search').val();
        formData['filter'] = filter;
        formData['pera'] = $('#pera').val();
        formData['sectionFilter'] = $('#sectionName').val();
        formData['page'] = 1;
        $("#exportAction").val(filter);
        $.ajax({
            type: "POST",
            url: '/eventprogram/manage-list-of-videos',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
    
     manageBooth: function(filter,page){
        var formData = {};
        formData['search'] = $('#search').val();
        formData['filter'] = filter;
        formData['pera'] = $('#pera').val();
        formData['page'] = 1;
        
        $("#exportAction").val(filter);
        $.ajax({
            type: "POST",
            url: '/eventprogram/manage-booth-list',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
    
    manageBoothMedia: function(filter,page){
        var formData = {};
        formData['search'] = $('#search').val();
        formData['filter'] = filter;
        formData['pera'] = $('#pera').val();
        formData['page'] = 1;
        
        $("#exportAction").val(filter);
        $.ajax({
            type: "POST",
            url: '/eventprogram/manageBoothMediaList',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
    
    
    manageBoothLargeMedia: function(filter,page){
        var formData = {};
        formData['search'] = $('#search').val();
        formData['filter'] = filter;
        formData['pera'] = $('#pera').val();
        formData['page'] = 1;
        
        $("#exportAction").val(filter);
        $.ajax({
            type: "POST",
            url: '/eventprogram/manageBoothLargeMediaList',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
     experienceZone: function(filter,page){
        var formData = {};
        formData['search'] = $('#search').val();
        formData['filter'] = filter;
        formData['pera'] = $('#pera').val();
        formData['page'] = 1;
        
        $("#exportAction").val(filter);
        $.ajax({
            type: "POST",
            url: '/eventprogram/experience-zone-video-list',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
    
};      
//method to append data  
function appendUserDetail(username,type){
  $("#usernameonw").val("");
  $("#usernameonw").val(username);
  $.ajax({
            type: "POST",
            url: '/eventprogram/get-user-detail',
            data: {username:username,type:type},
            success: function (data) {
                    if(type=="attende"){
                     $('#EditAttendeeModal .transport').html("");
                     $('#EditAttendeeModal .transport').html(data);
                   }else{
                     $('#EditSpeakerModal .transport1').html("");
                     $('#EditSpeakerModal .transport1').html(data);  
                   }
            },
                    
               failure: (function () { alert("Failure!!"); })
            });    
     
 }
 //method to append data  
function appendNotification(username){
  $("#usernameonw").val("");
  $("#usernameonw").val(username);
  $.ajax({
            type: "POST",
            url: '/eventprogram/append-notification',
            data: {username:username},
            success: function (data) {
                     $('#SendPushModal .appendNot').html("");
                     $('#SendPushModal .appendNot').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }
 
 
 function sendemailapplaunch(){
   var formData = {};
        formData['search'] = $('#search').val();
        formData['guestFilter'] = $('#guestFilter').val();
        formData['subCategoryFilter'] = $('#subCategoryFilter').val();
        formData['cityFilter'] = $('#cityFilter').val();
        formData['loginUser'] = $('#loginUser').val();
        formData['firstNameOrder'] = $("#firstNameOrder").val();
        formData['toDate'] = $('#toDate').val();
        formData['fromDate'] = $('#fromDate').val();
        formData['page'] = '1';
        formData['filter'] = $("#role").val();
        formData['pera'] = $('#pera').val();
        formData['templateId'] = $("#templateId").val();
        $.ajax({
            type: "POST",
            url: '/event/sendemailapplaunch',
            datatype:'json',
            data: formData,
            async: true,
            success:function(data,status){
            },
            failure: (function () { alert("Failure!!"); })
      });  
 }
 function deleteSpeaker(userId){
   var filter = $("#role").val();
   var page = $("#currentPage").val(); 
            var jobdata;
       var r = confirm("Confirm that you want Delete Speaker ?");
        if (r == true) {
             jobdata = {userId:userId};
        }  else {
            return false;
        }  
        $.ajax({
            type: "POST",
            url: '/eventprogram/delete-speaker',
            datatype:'json',
            data: jobdata,
            success: function (data) {
              drooly.getSpeakerList(filter,page);
          },
                    
                failure: (function () { alert("Failure!!"); })
            });
        }
        
        
    function ActiveBooth(boothId,type){
//       var jobdata;
//        alert(type); return false;    
       if(type=="2"){   
       var r = confirm("Confirm that you want Inactive this Booth ?");
//        $(this).val('FALSE');
//     $('#customCheck2').removeProp('checked'); 
//           $("#customCheck1").prop( "checked", false );       
   }else{
       var r = confirm("Confirm that you want Active this Booth ?");
//         $(this).val('TRUE'); 
//         $('#customCheck1').prop('checked', true);
//          $("#customCheck1").prop( "checked", true );
   }
   
   
   
//   if($(this).attr('checked')){
//          $(this).val('TRUE');
//     }else{
//          $(this).val('FALSE');
//     }
   
        $.ajax({
            type: "POST",
            url: '/eventprogram/active-booth',
            datatype:'json',
            data: {boothId:boothId,type:type},
            success: function (data) {
//                alert(data); return false;
              drooly.manageBooth();
               },
            failure: (function () { alert("Failure!!"); })
            });
        }    
        
        
    //method to append data  
function addVideos(type){
//  $("#usernameonw").val("");
  var section=$("#sectionName").val();
//  alert(section); return false;
  $.ajax({
            type: "POST",
            url: '/eventprogram/addVideo',
            data: {type:type,section:section},
            success: function (data) {
                
//                alert(data); return false;
                     $('#addVideoModal .saveVideo').html("");
                     $('#addVideoModal .saveVideo').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }  
 
 
 
 function addMediaForExperienceZone(type){
//  $("#usernameonw").val("");
//  $("#usernameonw").val(username);
  $.ajax({
            type: "POST",
            url: '/eventprogram/addVideoForExperienceZone',
            data: {type:type},
            success: function (data) {
                
//                alert(data); return false;
                     $('#addVideoModal .saveVideo').html("");
                     $('#addVideoModal .saveVideo').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }  
 
 function addBoothData(type){
//  $("#usernameonw").val("");
//  $("#usernameonw").val(username);
  $.ajax({
            type: "POST",
            url: '/eventprogram/addBoothData',
            data: {type:type},
            success: function (data) {
                
//                alert(data); return false;
                     $('#addVideoModal .saveVideo').html("");
                     $('#addVideoModal .saveVideo').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 } 
 
 
 
 
  function addBoothsMedia(boothId){
  $.ajax({
            type: "POST",
            url: '/eventprogram/addBoothMedia',
            data: {boothId:boothId},
            success: function (data) {
                
//                alert(data); return false;
                     $('#addVideoModal .saveVideo').html("");
                     $('#addVideoModal .saveVideo').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }  
 
 
 
 function addBoothsLargeMedia(boothId){
  $.ajax({
            type: "POST",
            url: '/eventprogram/addBoothLargeMedia',
            data: {boothId:boothId},
            success: function (data) {
                
//                alert(data); return false;
                     $('#addVideoModal .saveVideo').html("");
                     $('#addVideoModal .saveVideo').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 } 
 
 
        
  //delete record
function deleteRecord(type,id){
      var pushdata;
       var r = confirm("Confirm that you want to delete This Record ?");
        if (r == true) {
            pushdata = {type:type,id:id};
        }  else {
            return false;
        }     
        $.ajax({
            type: "POST",
            url: '/eventprogram/delete-record',
            data: pushdata,
            datatype:'json',
            success: function (data) {  
             if(type=="agenda"){   
              drooly.manageAgenda('all','0');
             }else if(type=="nightfest"){
              drooly.manageNightFest('all','0');   
             }else{
               drooly.managePartners('all','0');   
             }
            },
                    
                failure: (function () { alert("Failure!!"); })
            });    
                
  }
  
  
  function deleteVideos(id,type){
    var jobdata;
  if(type=="2"){   
       var r = confirm("Confirm that you want to Inactive this media ?");
      $("#customCheck1").prop("checked", true); 
      $("#customCheck2").prop("checked", false);  
       
   }else{
       var r = confirm("Confirm that you want to Active this media ?");
       $("#customCheck2").prop("checked", true); 
       $("#customCheck1").prop("checked", false); 
       
       
   }
        if (r == true) {
             jobdata = {id:id,type:type};
        }  else {
            return false;
        }  
        $.ajax({
            type: "POST",
            url: '/eventprogram/deleteVideo',
            datatype:'json',
            data: jobdata,
            success: function (data) {
//                alert(data); return false;
              drooly.manageVideos();
          },
                    
                failure: (function () { alert("Failure!!"); })
            });
        }
        
        
      function removeVideo(id){
    var jobdata;
       
       var r = confirm("Confirm that you want to delete this media ?");
        if (r == true) {
             jobdata = {id:id};
        }  else {
            return false;
        }  
        $.ajax({
            type: "POST",
            url: '/eventprogram/removeVideo',
            datatype:'json',
            data: jobdata,
            success: function (data) {
//                alert(data); return false;
              drooly.manageVideos();
          },
                    
                failure: (function () { alert("Failure!!"); })
            });
        }   
        
        
        
        
  function deleteBoothMedia(id,type){
    var jobdata;
  if(type=="2"){   
       var r = confirm("Confirm that you want to Inactive this media ?");
      $("#customCheck1").prop("checked", true); 
      $("#customCheck2").prop("checked", false);  
       
   }else{
       var r = confirm("Confirm that you want to Active this media ?");
       $("#customCheck2").prop("checked", true); 
       $("#customCheck1").prop("checked", false); 
       
       
   }
        if (r == true) {
             jobdata = {id:id,type};
        }  else {
            return false;
        }  
        $.ajax({
            type: "POST",
            url: '/eventprogram/deleteBoothMedia',
            datatype:'json',
            data: jobdata,
            success: function (data) {
//                alert(data); return false;
              drooly.manageBoothMedia();
          },
                    
                failure: (function () { alert("Failure!!"); })
            });
        } 
        
        
    function deleteBoothLargeMedia(id,type){
    var jobdata;
  if(type=="2"){   
       var r = confirm("Confirm that you want to Inactive this media ?");
      $("#customCheck1").prop("checked", true); 
      $("#customCheck2").prop("checked", false);  
       
   }else{
       var r = confirm("Confirm that you want to Active this media ?");
       $("#customCheck2").prop("checked", true); 
       $("#customCheck1").prop("checked", false); 
       
       
   }
        if (r == true) {
             jobdata = {id:id,type};
        }  else {
            return false;
        }  
        $.ajax({
            type: "POST",
            url: '/eventprogram/deleteBoothLargeMedia',
            datatype:'json',
            data: jobdata,
            success: function (data) {
//                alert(data); return false;
              drooly.manageBoothLargeMedia();
          },
                    
                failure: (function () { alert("Failure!!"); })
            });
        }      
        
        
        
     function deleteExperienceZoneMedia(id,type){
    var jobdata;
  if(type=="2"){   
       var r = confirm("Confirm that you want to Active this media ?");
   }else{
       var r = confirm("Confirm that you want to Active this media ?");
   }
        if (r == true) {
             jobdata = {id:id,type};
        }  else {
            return false;
        }  
        $.ajax({
            type: "POST",
            url: '/eventprogram/deleteVideoForExperienceZone',
            datatype:'json',
            data: jobdata,
            success: function (data) {
//                alert(data); return false;
              drooly.experienceZone();
          },
                    
                failure: (function () { alert("Failure!!"); })
            });
        }   
        
  
  
  //method to append data  
function appendAgenda(agendaId){
  $.ajax({
            type: "POST",
            url: '/eventprogram/append-agenda',
            data: {agendaId:agendaId},
            success: function (data) {
                     $('#editAgendaModal .appendAgenda').html("");
                     $('#editAgendaModal .appendAgenda').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }
  //method to append data  
function appendAddAgenda(){
  $.ajax({
            type: "POST",
            url: '/eventprogram/append-add-agenda',
            data: {agendaId:'newAgenda'},
            success: function (data) {
                     $('#AddAgendaModal .appendAddAgenda').html("");
                     $('#AddAgendaModal .appendAddAgenda').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }
 //method to append data  
function appendscspeaker(agendaId,action){
  $.ajax({
            type: "POST",
            url: '/eventprogram/appendscspeaker',
            data: {agendaId:agendaId,action:action},
            success: function (data) {
                     $('#addScSpeakerModal .addScSpeaker').html("");
                     $('#addScSpeakerModal .addScSpeaker').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }
 
 
function appendAgendaConference(agendaId){
  $.ajax({
            type: "POST",
            url: '/eventprogram/append-agenda-conference',
            data: {agendaId:agendaId},
            success: function (data) {
                     $('#editAgendaModalConference .appendAgendaConference').html("");
                     $('#editAgendaModalConference .appendAgendaConference').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }
 function startCounter(id){
    var jobdata;
       var r = confirm("Confirm that you want to Start Counter ?");
       if (r == true) {
             jobdata = {id:id};
        }  else {
            return false;
        }  
        $.ajax({
            type: "POST",
            url: '/eventprogram/startagendacounter',
            datatype:'json',
            data: jobdata,
            success: function (data) {
                alert("Counter Started Successfully"); return true;
              
          },
                    
                failure: (function () { alert("Failure!!"); })
            });
        } 