 function shortListUserData(userId,job_id,id,action="",page=""){
      $.ajax({
            type: "POST",
            url: '/users/setShortlistCandidate',
            data: {userId:userId,job_id:job_id,id:id},
            datatype:'json',
           beforeSend: function (data) {
                $("#startup").show();
            },  
            success: function (data) {
                   $("#startup").hide();
                if(data.send.status==1){
                    
                     bootbox.alert({
                    message: data.send.message,
                    size: 'large'
                  });
                    
                    if(action=="match" && page=="matchingApplicant"){
                      drooly.MatchingApplicantData();
                      return true;
                    }else if(action=="match" && page=="matchingApplicantCandidate"){
                     window.location.href = "/users/matchingApplicant/"+job_id;
                     return true;
                    } else if(action=="match" && page=="matchingCandidates"){
                       jobmatches('match',1);
                       return true;
                    }else if(action=="match" && page=="matchingCandidatesDetail"){
                        window.location.href = "/users/searchCandidates";
                        return true;
                    }else if(action=="match" && page=="jobViewDetail"){
                        drooly.ViewDetailData();
                        return true;
                    }else if(action=="match" && page=="candidateJobProfile"){
                        window.location.href = "/roles/jobViewDetails/"+job_id;
                        return true;
                    }else{
                      console.log("something went wrong"); return true; 
                   }
                   
                }else if(data.send.status==5){
                     bootbox.alert({
                    message: data.send.message,
                    size: 'large'
                  });
                     window.location.href = "/subscription/subplans"
               
                }else {
                    console.log(data.send.message);
               }
            },
             failure: (function () { alert("Failure!!"); })
        });    
                
  }
  
  function interviewoffered(userId,job_id,id,action="",page=""){
        $.ajax({
            type: "POST",
            url: '/users/offerinterview',
           data: {userId:userId,job_id:job_id,id:id},
           datatype:'html',
           beforeSend: function (data) {
                $("#startup").show();
           },  
           success: function (data) {
                $("#startup").hide();
                $('#offerInterview .offerint').html(data);     
                  $("#interviewaction").val(action);
                  $("#interviewpage").val(page);
            },
           failure: (function () { alert("Failure!!"); })
        });           
  }
 function setInterview(){
    var today = new Date();
    var TodayDate = today.setHours(0,0,0,0);
    var endDate= $("#datepicker").val();
    var compareDate = new Date(endDate);
    var newDate     = compareDate.setHours(0,0,0,0);
    if(endDate==""){
          bootbox.alert({
            message: "Interview Date is Requird",
            size: 'large'
         });
        return false;
   }
  if (newDate < TodayDate) {
            bootbox.alert({
            message: "Interview date must be current date or higher",
            size: 'large'
         });
        return false;
  } 
     var action = $("#interviewaction").val();
     var page = $("#interviewpage").val();
     var time = $("#time").val();
     var interviewtype = document.querySelector('input[name="selector"]:checked').value;
     var username = $("#candidateid").val();
     var jobid    = $("#interviewid").val();
     $.ajax({
            type: "POST",
            url: '/users/setInterviewOfferJob',
           data: {date:endDate,time:time,interviewtype:interviewtype,username:username,jobid:jobid},
           beforeSend: function (data) {

                $("#startup").show();
            },  
            success: function (data) {
//                alert(data); return false;
                $("#startup").hide();
                $('#offerInterview').modal('hide');
              if(data=="success"){

                  bootbox.alert({
                    message: "You have offered an interview successfully.",
                    size: 'large'
                        });
                  
                if(action == 'shortlist' && page=='shortlistCandidate'){  
                     jobmatches('shortlist','all');
                     return true;
                } else if(action == 'shortlist' && page=='shortlistCandidateDetail') {
                    window.location.href = "/users/searchCandidates";
                    return true;
                } else if(action == 'shortlist' && page=='jobViewDetail'){
                    drooly.ViewDetailData();
                    return true;
                }else if(action == 'shortlist' && page=='candidateJobProfile'){
                     window.location.href = "/roles/jobViewDetails/"+jobid;
                     return true;
                }else {
                      console.log("something went wrong"); return true; 
                }
                   
              }
              else{
                  
                   bootbox.alert({
                    message: "oops! Your Subscription has been Expired",
                    size: 'large'
                        })
                     window.location.href = "/subscription/subplans"
              }
            },
           failure: (function () { alert("Failure!!"); })
        });    
 } 
function jobOffered(userId,job_id,id,action="",page =""){
        $.ajax({
            type: "POST",
            url: '/users/offerJob',
           data: {userId:userId,job_id:job_id},
            datatype:'html',
             beforeSend: function (data) {
                $("#startup").show();
            },
            success: function (data) {
               $("#startup").hide();
              if(data=="success"){
              bootbox.alert({
                    message: "You Have Offered a Job Successfully",
                    size: 'large'
                        });
                if(action == 'offerinterview' && page=="interviews"){
                  jobmatches('interview','all');
                  return true;
                } else if(action == 'offerinterview' && page=="interviewsDetail"){
                    window.location.href = "/users/searchCandidates";
                    return true;
                } else if(action == 'offerinterview' && page=="jobViewDetail"){
                     drooly.ViewDetailData();
                     return true;
                }else if(action == 'offerinterview' && page=="candidateJobProfile"){
                     window.location.href = "/roles/jobViewDetails/"+job_id;
                     return true;
                }else {
                      console.log("something went wrong"); return true; 
                }
                
             } else{
                  
                     bootbox.alert({
                    message: "oops! Your Subscription has been Expired",
                    size: 'large'
                        });
                    window.location.href = "/subscription/subplans"
             }     
           },
          failure: (function () { alert("Failure!!");})
        });           
  }