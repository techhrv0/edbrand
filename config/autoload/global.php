<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'settings' => array(
         'quickPole'=>"http://local.techhr.com/",   
         'PUSH_FILE' => __DIR__.'/../../data/push/riyalApp.pem',
        'DEFAULT_PROFILE_PICS_LOCATION' => __DIR__ . '/../../public/uploadedFiles/defaultPics/',
        'POST_IMAGE_FILE_PATH' => __DIR__ .
        '/../../public/uploadedFiles/postImage/',
        'PROFILE_PIC_FONT_LOCATION' => __DIR__ . '/../../public/fonts/',
        "COMMON_SALT" => '8upJPwQVqgKsLWCCEYjBf6gdvxdDuTnEZ638F2DahsyKTmhzwLXGgzwdtBCNW6DC7bNaEkHTP7Qd8Nra74VcAaRLAJw6Wpf6wvfpPsnpv2a9MjH2LYF2jAFLVkJuVrC6',
        'IMPORT_FILE_MAX_SIZE' => '999437184',
        'FILE_IMPORT_EXTENSION' => array('xls', 'xlsx', 'jpg', 'jpeg',
            'png', 'gif', 'XLS', 'XLSX', 'JPG', 'JPEG', 'PNG', 'GIF'),
        'IMAGE_EXTENSION' => array('jpg', 'jpeg',
            'png','JPG', 'JPEG', 'PNG'),
        'FILE_IMPORT_MIME' => array('application/excel',
            'application/vnd.ms-excel', 'application/x-excel',
            'application/x-msexcel', 'application/msexcel',
            'application/x-ms-excel', 'application/x-dos_ms_excel',
            'application/xls', 'application/x-xls',
            'application/vnd.oasis.opendocument.spreadsheet',
            'text/csv', 'application/csv',
            'application/octet-stream', 'application/zip',
            'application/msword',
            'application/vnd.oasis.opendocument.text', 'image/gif',
            'image/jpeg', 'image/jpg', 'application/pdf',
            'application/msword',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-powerpoint',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'image/jpeg'),
        'PROFILE_IMAGE_FILE_PATH' => __DIR__ .
        '/../../public/uploadedFiles/profileImage/',
        'NEW_IMAGE_LOCATION' => 'uploadedFiles/profileImage/',
        'MEDIA_FILE_PATH' => __DIR__ .
        '/../../public/uploadedFiles/mediaContent/',   
        'MEDIA_FILE_PATH1' => __DIR__ .
        '/../../public/uploadedFiles/pdf/',
        'IMPORT_VIDEO_MAX_SIZE' => '915728640',
        'UPLOAD_VIDEO_EXTENSION' => array('3gp', '3gpp', 'mp4', 'flv', '3GP', '3GPP', 'MP4', 'FLV',
            'mpeg', 'mpeg4', 'mkv', 'dat', 'avi', 'mov',
            'MPEG', 'MPEG4', 'MKV', 'DAT', 'AVI', 'MOV',
            'wmv', 'mpg', 'WMV', 'MPG'),
        'UPLOAD_AUDIO_EXTENSION' => array('mp3', 'arm', 'm4a', 'MP3', 'ARM', 'M4A'),
         "defaultUrl"=>"http://local.nickyanka.com",
   'SMS' => array(
            'NUMBER'=> '+15874001739',
            'SID'   => 'ACe399faf9090602cc3562766c5bf6471f',
            'TOKEN' => '0d01dfd0e9cf8de3581dc5256b6b4292',
         )
    ),
    "S3BUCKET" => array(
            'AWSKEY' => 'AKIAJE5EF2ZKICUUBH4Q',
            'AWSSECRET' => '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',
            'FILEBUCKET' => 'riyalstory',
            'BUCKETURL' => 'https://s3-eu-west-1.amazonaws.com/riyalstory/',
        ),
    'USER_DEFAULT_SETTINGS' => array(
        'NOTIFICATION_SWITCH' => '1',
        'MESSAGE_NOTIFICATION' => array(
            "SOUNDNAME" => "Default",
            "ALERT" => 1,
            "SHOW_PREVIEW" => 1,
        ),
    ),
    'session_config' => array(
    'name' => 'your session name',
    'remember_me_seconds' => 60 * 60 * 24,
    'use_cookies' => true,
    'cookie_httponly' => true,
),
);