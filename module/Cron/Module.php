<?php
namespace Cron;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
//   public function getServiceConfig() {
//        return array(
//            'factories' => array(
//                'Posts\Model\PostTable' => function ($sm) {
//                    $adapter = $sm->get('Zend\Db\Adapter\Adapter');
//                    $table = new PostTable($adapter);
//                    return $table;
//                },
//               'Posts\Model\PropertydescriptionTable' => function ($sm) {
//                    $adapter = $sm->get('Zend\Db\Adapter\Adapter');
//                    $table = new PropertydescriptionTable($adapter);
//                    return $table;
//                },         
//            )
//        );
//    }

}
