<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Cron\Controller\Cron' => 'Cron\Controller\CronController',
        ),
    ),
  'console' => array(
    'router' => array(
        'routes' => array(
            'cron' => array(
                'options' => array(
                    'route'    => 'cron (updateuser):task',
                    'defaults' => array(
                        'controller' => 'Cron\Controller\Cron',
                        'action'     => 'cron'
                    )
                )
              ),
           ),
         ),
    ), 
//    'router' => array(
//        'routes' => array(
//            'cron' => array(
//                'type'    => 'segment',
//                'options' => array(
//                    'route'    => '/cron[/][:action][/:id]',
//                    'constraints' => array(
//                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                    ),
//                    'defaults' => array(
//                        'controller' => 'Cron\Controller\Cron',
//                        'action'     => 'cron',
//                    ),
//                ),
//            ),
//        ),
//    ),
    
    
//    'view_manager' => array(
//        'template_path_stack' => array(
//            'index' => __DIR__ . '/../view',
//        ),
//        'strategies' => array (
//            'ViewJsonStrategy'
//        )
//    ),
);