<?php
namespace Users\Controller;
use Zend\View\Model\ViewModel;
use CustomLib\Controller\CustomController;
use Zend\View\Model\JsonModel;
class EventController extends CustomController
{
 //default action control  
//method for gege guest    
public function manageAppLaunchAction(){
  $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable');
  $loginDetails = $this->getLoggedInUserId();
  $userCounters = $this->getUserCounter($loginDetails['testAccount']);
  $cityFilter = $usrTlb->getCityFilter($loginDetails['testAccount']);
//  $mail['mailFromNickName'] = "People Matters TechHR Conference";
//      $mail['mailTo'] = "bhupendrasingh.iimt@gmail.com";
//      $mail['addCc'] = "palak@approutes.com";      
//      $mail['mailSubject'] = "People Matters TechHR Singapore Conference App OTP for Login";
//      $msg = "<html><body>Dear User,<br><br>Thank you so much for downloading the People Matters TechHR Singapore Conference App.<br><br>Your Mobile App Login OTP is ".$code.". <br><br>Please enter this code in the OTP Code Box listed on the page to access the People Matters TechHR Singapore Conference App.<br><br>Please do not disclose/share your OTP with anyone for security reasons.<br><br>We look forward to seeing you at the conference.<br><br>Best Regards,<br>TechHR Singapore Team.</body></html>"; 
//      $mail['mailBody'] = $msg;
//      $checkResponse = $this->sendotpemail($mail);
//      echo "<pre>"; print_r($checkResponse); die;
  
  $pera = $this->params()->fromroute('id', NULL);
  if(!empty($pera)){
      $idArray = explode("-", base64_decode($pera));
      $filter = $idArray[0];
      $page = $idArray[1];
      $lastUserId = $idArray[2];
   }else{
     $page =0;
     $filter ='all';
     $lastUserId = 0;
  } 
   $view = new ViewModel(array('userCounter'=>$userCounters,'page'=>$page,'filter'=>$filter,'lastUserId'=>$lastUserId,'cityFilter'=>$cityFilter));
   return $view;
 }
 //get counters For guest acc to dates:- 
 public function getUserCounter($testAccount){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable'); 
    $counters['all']    = $usrTlb->getCounterOfUsers('all',$testAccount);  
    $counters['VIP']   = $usrTlb->getCounterOfUsers('VIP',$testAccount);
    $counters['HRPRAC']   = $usrTlb->getCounterOfUsers('HRPRAC',$testAccount);
    $counters['SERVICEPRO']   = $usrTlb->getCounterOfUsers('SERVICEPRO',$testAccount);
    $counters['totalLogin']   = $usrTlb->getCounterOfUsers('totalLogin',$testAccount);
    $counters['iosLogin']   = $usrTlb->getCounterOfUsers('iosLogin',$testAccount);
    $counters['androidLogin']   = $usrTlb->getCounterOfUsers('androidLogin',$testAccount);
    $counters['notLogin']   = $usrTlb->getCounterOfUsers('notLogin',$testAccount);
    return $counters;
 }
 // for all users
 public function manageAppLaunchDataAction(){
  $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable'); 
  $request = $this->getRequest();
  $data = $request->getPost()->toArray();
  $loginDetail = $this->getLoggedInUserId();
  $paginator = $usrTlb->getAllUsers($data,true,$loginDetail['testAccount']);
  $results = $this->paginationToArray($paginator,$data['page'],4);
  foreach($results['allData'] as $key=>$value){
      $results['allData'][$key]['date'] = date("d-m-Y",$value['uts']);
  }
  $uri = $this->getRequest()->getUri();
  $mediaUrl = $this->getMediaUrl();
  $view = new ViewModel(array(
          'paginator' => $paginator,
          'uri' => $uri,
          'results' => $results,
          'mediaUrl'=>$mediaUrl
     ));
      $view->setTerminal(true);
      return $view;
}
//method for send app launch email to user
public function sendemailapplaunchAction(){
   $request = $this->getRequest();
   $data = $request->getPost()->toArray();
   $loginDetail = $this->getLoggedInUserId();
   //code for job que
   $config = $this->getServiceLocator()->get('config');    
   $jobData['url'] = $config['settings']['JobQueUrl'].'/event/sendemailque';
   $jobData['jobClass'] = 'JobQueue';
   $jobData['queue'] = 'SpecialJobQ';
   $jobData['time'] = time();
   $jobData['testAccount'] = $loginDetail['testAccount'];
   $jobData['cityFilter'] = $data['cityFilter'];
   $jobData['firstNameOrder'] = $data['firstNameOrder'];
   $jobData['toDate'] = $data['toDate'];
   $jobData['fromDate'] = $data['fromDate'];
   $jobData['page'] = $data['page'];
   $jobData['filter'] = $data['filter'];
   $jobData['templateId'] = $data['templateId'];
   $this->insertJobQueue($jobData);
   return new JsonModel(array('message'=>"success"));
}
//method for sending email in que

public function sendemailqueAction(){
 $request = $this->getRequest();
 $data = $request->getPost()->toArray();
 $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable');
 $userList = $usrTlb->getAllUsers($data,false,$data['testAccount']);
 foreach($userList as $value){ 
     $mail['mailFromNickName'] = "EvenTap";
    // $mail['mailTo'] = "bhupendra@approutes.com";
     $mail['mailTo'] = $value['email'];      
     $mail['mailSubject'] = "EvenTap App Launch";
     $mail['mailBody'] = $this->otpmailtemplate($value,$data['templateId']);
     $this->sendotpemail($mail);
 }
 echo "success"; exit;   
}
//code for send email by otp
private function sendotpemail($mail){
$email = new \SendGrid\Mail\Mail(); 
$email->setFrom("support@eventap.co", "EvenTap");
$email->setSubject($mail['mailSubject']);
$email->addTo($mail['mailTo']);
$email->addContent(
    "text/html", $mail['mailBody']
);
$full_key ="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"; 
$sendgrid = new \SendGrid($full_key);
try {
    $response = $sendgrid->send($email);
     return "success";
   // print $response->statusCode() . "\n";
   // print_r($response->headers());
   // print $response->body() . "\n";
    //die("hello");
} catch (\Exception $e){
   return "failure";
   //echo 'Caught exception: '. $e->getMessage() ."\n";
   //die("hello");
}
}
public function otpmailtemplate($userDetail,$templateId){
      $view = $this->getServiceLocator()->get('ViewRenderer');
      $viewModel = new ViewModel();
      $viewModel->setVariables(array('userDetail'=>$userDetail));
      if($templateId=="deafult"){
       $viewModel->setTemplate('application/index/otpmailtemplate');
      }
      $content = $view->render($viewModel);
      return $content;
  }
}