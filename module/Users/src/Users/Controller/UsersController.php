<?php
namespace Users\Controller;
use Zend\View\Model\ViewModel;
use CustomLib\Controller\CustomController;
use Zend\View\Model\JsonModel;
//use CustomLib\Service\ApiMessages;
use Zend\Filter\StripTags;
use Zend\Filter\StringTrim;
class UsersController extends CustomController
{
  //default action control  
  public function indexAction()
  {
    return new ViewModel(array());
  }
 public function notificationAction(){
    $eventTable = $this->getServiceLocator()->get('Application\Model\EventTable');
    $dateFilter = $eventTable->getDateListofNotifiction();
    $viewModel = new ViewModel(array('dateFilter'=>$dateFilter));
    return $viewModel;   
 }
 //method for notfication data list
 public function notificationDataAction(){
     $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
     $request = $this->getRequest();
     $loginDetail = $this->getLoggedInUserId();
     $data = $request->getPost()->toArray();
     if(!empty($data['fromDate'] && $data['toDate'])){
     $data['fromDate'] = $this->timzeconvertionNew($data['fromDate']);
     $data['toDate'] = $this->timzeconvertionNew($data['toDate']);
     }
     $data['testAccount']=$loginDetail['testAccount'];

     $paginator = $usrTlb->getAllNotification($data,true);
     $results = $this->paginationToArray($paginator,$data['page'],5);
     foreach ($results['allData'] as $key=>$value){
         $results['allData'][$key]['createdOn'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['createdOn']),$loginDetail['timeZone'],3);
         $results['allData'][$key]['totalSend'] = $usrTlb->getProfileCounter('notification',"notificationDataId='".$value['id']."'",0);
         $results['allData'][$key]['totalView'] = $usrTlb->getProfileCounter('notification',"notificationDataId='".$value['id']."' and isView=1",0);
     }
     //echo "<pre>"; print_r($results); die;
       $mediaUrl = $this->getMediaUrl();
       $uri = $this->getRequest()->getUri();
       $view = new ViewModel(array(
              'paginator' => $paginator,
              'uri' => $uri,
              'results' => $results,
              'mediaUrl'=>$mediaUrl
          ));
          $view->setTerminal(true);
          return $view;   
   }
//method for add notfication
 public function addNotificationAction() {
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $loginDetail = $this->getLoggedInUserId();
    if($request->isPost()) {
      $data = $request->getPost()->toArray();
     //data store in notification data table 
      $notfiyData['title'] = $data['title'];
      //$notfiyData['message'] =  $data['msg'];
      if(($data['uiType']=='default') || ($data['uiType']=='web')){
       $html ='<font size="16">'.$data['msg'].'</font>';  
       $htmlMessage =$data['title'];   
      }else{
       $html = "";
       $htmlMessage = $data['msg'];   
      }
      $notfiyData['message'] =  $htmlMessage;
      $notfiyData['createdOn'] = time();
      $notfiyData['toSide'] =   $data['usergroup'];
      $notfiyData['testAccount'] = $loginDetail['testAccount'];
      
      if($data['save']=='send'){
       $notfiyData['type'] =   "send";
      }else {
       $notfiyData['type'] =   "save";   
      }
      
      if(!empty($data['link'])){
       $notfiyData['link'] = $data['link'];
      }else{
       $notfiyData['link'] = "";   
      }
      
      
      if($data['uiType']=="web"){
      $notfiyData['uiType'] ="default";
      }else{
       $notfiyData['uiType'] = $data['uiType'];    
      }
      
      
      $notfiyData['html'] = $html;
      $notificationDataId = $usrTlb->saveData('notificationData',$notfiyData);
     //save file of notfication sol media  table 
     $files = $request->getFiles()->toArray();
    if(!empty($files['attachment'])){
      foreach($files['attachment'] as $val){
        //new code for file uploading
         $mime = $val['type'];
        if(strstr($mime, "video/")){
            //code for video upload
            $destination = "public/uploadedFiles/OstrichMedia/";
            $neoFileName = "notification/".uniqid('notif').'.mp4';
            $file = move_uploaded_file($val['tmp_name'],$destination.$neoFileName);
            if($file){
                $frame = $this->getVideoFrame($destination ,$neoFileName);
                $resizeImage110 = $this->reSizeImageforpost($destination, $frame, 720, 1280,15);
                $status = $this->putIntoS3Bucket($destination.$neoFileName,$neoFileName,2);
                $status1 = $this->putIntoS3Bucket($destination . $resizeImage110, 'small'.$frame,1);
                $status2 = $this->putIntoS3Bucket($destination . $frame, 'large'.$frame,1);
                if($status1 =='success' && $status2=='success' && $status =='success'){
                  $thumb   = $frame; 
                  unlink($destination . "/" . $frame);
                  unlink($destination . "/" . $resizeImage110);
                  unlink($destination.$neoFileName);
                  $usrTlb->saveData('notificationMedia',array('notificationDataId'=>$notificationDataId,'media'=>$neoFileName,'thumbNail'=>$thumb,'type'=>2,'creadtedBy'=>'admin','updatedBy'=>'admin','createdOn'=> time(),'updatedOn'=> time()));     
               }else{
                 unlink($destination . "/" . $frame);
                 unlink($destination . "/" . $resizeImage110);
                 unlink($destination.$neoFileName);  
               } 
          }
                         
     }else if(strstr($mime, "image/")){
             $image = "notification/".uniqid('notif').'.png';
             $success = $this->putIntoS3Bucket($val['tmp_name'],$image,1);
             if($success=="success"){
                $usrTlb->saveData('notificationMedia',array('notificationDataId'=>$notificationDataId,'media'=>$image,'thumbNail'=>"",'type'=>1,'creadtedBy'=>'admin','updatedBy'=>'admin','createdOn'=> time(),'updatedOn'=> time()));
             }
             
          }else{
            //do it later   
          }   
       } 
   }
   if(!empty($data['link'])){
        $link = $data['link'];
     }else{
        $link = "";   
     }
   //code for job que
   $config = $this->getServiceLocator()->get('config');    
   $jobData['url'] = $config['settings']['JobQueUrl'].'/application/index/sendnotificationpush';
   $jobData['jobClass'] = 'JobQueue';
   $jobData['queue'] = 'SpecialJobQ';
   $jobData['time'] = time();
   $jobData['notificationDataId'] = $notificationDataId;
   $jobData['testAccount'] = $loginDetail['testAccount'];
   $jobData['uiType'] = $data['uiType'];
   $jobData['link'] = $link;
   $this->insertJobQueue($jobData);
   $this->redirect()->toUrl("/users/notification");
   }
   return new ViewModel(array());
 }
  //send notification to user
 public function sendNotificationAction(){    
   //$usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $request = $this->getRequest();
   $data = $request->getPost()->toArray();
   //$notificationData = $usrTlb->getNotification($data);
  // $username = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
   $loginDetails = $this->getLoggedInUserId();
   //code for job que
   $config = $this->getServiceLocator()->get('config');    
   $jobData['url'] = $config['settings']['JobQueUrl'].'/application/index/sendnotificationpush';
   $jobData['jobClass'] = 'JobQueue';
   $jobData['queue'] = 'SpecialJobQ';
   $jobData['time'] = time();
   $jobData['notificationDataId'] = $data['id'];
   $jobData['testAccount'] = $loginDetails['testAccount'];
   $this->insertJobQueue($jobData);
   echo "success"; exit;     
 }   
  public function socialPostAction(){
   $viewModel = new ViewModel();
   return $viewModel;      
  } 
  public function getSocialpostDataAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $loginDetail = $this->getLoggedInUserId();
    $paginator = $usrTlb->getAllSocialPost($data,true,$loginDetail['testAccount']);
    $results = $this->paginationToArray($paginator,$data['page'],20);
    foreach($results['allData'] as $key=>$value){
        $results['allData'][$key]['date'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['createdOn']),$loginDetail['timeZone'],1);
    }
    $uri = $this->getRequest()->getUri();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results
       ));
        $view->setTerminal(true);
        return $view;
   }  
  public function socialPostMediaAction(){
   $pera = $this->params()->fromroute('id', NULL);
   $viewModel = new ViewModel(array('pera'=>$pera));
   return $viewModel;      
   }

 public function viewPostMediaAction(){
      $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
      $request = $this->getRequest();
      $data = $request->getPost()->toArray();
      $data['socialPostId']= base64_decode($data['pera']);
      $paginator = $usrTlb->getSocialPostMedia($data,true);
      $results = $this->paginationToArray($paginator,$data['page'],20);
      $loginDetail = $this->getLoggedInUserId();
      foreach($results['allData'] as $key=>$value){
      $results['allData'][$key]['date'] = $this->getUserTimeZone($value['timeStamp'],$loginDetail['timeZone'],1);
      }
    $mediaUrl=$this->getMediaUrl();
    $uri = $this->getRequest()->getUri();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
             'mediaUrl'=>$mediaUrl
      ));
    $view->setTerminal(true);
    return $view;    
 }
 public function approveSocialPostAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $socialPostId= base64_decode($data['id']);
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
//    $usrTlb->updateData('socialPost',array('status'=>1),array('id'=> $socialPostId));
    $where3 = "id= '".$socialPostId."'"; 
    $getUserId=$usrTlb->getDetail('socialPost',array('userId'),$where3);
   
     $where2 = "username= '".$getUserId[0]['userId']."'"; 
     $getTestAccount = $usrTlb->getDetail('users',array('testAccount'),$where2);

    $where ="id=".$socialPostId."";
    $getPostData = $usrTlb->getDetail('socialPost',array('title','description'),$where);

    $where1 = "username='".$getUserId[0]['userId']."'";
    $getUserDetail = $usrTlb->getDetail('vcard_search',array('firstName'),$where1);
    $username = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
    $interData = array();
    $interData['title']=$getPostData[0]['description'];
    $interData['updatedon']=time();
    $interData['message']=$getPostData[0]['description'];
    $interData['notifyId']=$username;
    $interData['creatorId']=$getUserId[0]['userId'];
    $interData['eventId'] = $socialPostId;
    $interData['type']="adminPromoPost";
    $body1['message'] = $getUserDetail[0]['firstName']." has created a new  Post: ".$getPostData[0]['description']." . Click here to check";
    $body1['nickname'] = $getUserDetail[0]['firstName'];
    $body1['userId'] = $getUserId[0]['userId'];
    $body1['notifyId']=$username;
    $body1['eventName'] = $getPostData[0]['description'];
    $body1['eventId'] = $socialPostId;
    $body1['bodyType'] = "adminPromoPost";
    $body = json_encode($body1,true);
    $interData['body']=$body;
    //now make json to send users
     $message['badge'] = 1;
     $message['content-available'] =1;
     $message['bodyType'] = $interData['type'];
     $message['sender'] = "";
     $message['message']= $body;
     $message['timestamp'] = time();
     $payload['aps'] = $message;
     $payload['c'] = 1;
     $message1= "success";
     $getUserList = $usrTlb->getUserListToSendSocialPost($getUserId[0]['userId'],$getTestAccount[0]['testAccount']);
     foreach($getUserList as $list){
          $interData['username'] = $list['username'];
          $usrTlb->saveData('notification', $interData);
         if($list['device_type']=="iOS"){
              if(!empty($list["device_token"])){  
                   $checkLogin = $userTable->checkLogin($list['username'],$list["device_token"]);
                if(!empty($checkLogin)){
                 $this->newsendvoippush($payload,$message1,$list["device_token"],'dev');
                 //$this->newsendvoippush($payload,$message1,$list["device_token"],'dist');
               }
              }
               
         }else {
             if(!empty($list["device_token"])){  
                 $pushData = array("deviceId"=>$list["device_token"],
                                        "message"=>$body,
                                        'subject'=>"adminPromoPost",
                                        'bodyType'=>"adminPromoPost"
                                   );                    
                  $this->sendAndroidPush($pushData);
             
             }
                   
             }
     }
   return "success"; 
    
 }

 public function rejectSocialPostAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $usrTlb->updateData('socialPost',array('status'=>5),array('id'=> base64_decode($data['id'])));
    return new JsonModel(array('message'=>"success"));
 }
 
 
 public function deleteSocialPostAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $usrTlb->updateData('socialPost',array('status'=>4),array('id'=> base64_decode($data['id'])));
    return new JsonModel(array('message'=>"success"));
 }
 //method for current version for platform
 public function appversionAction(){
     return new ViewModel(array());
 }
//method for version of data list 
  public function appversiondataAction(){ 
        $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
        $loginDetail = $this->getLoggedInUserId();
        $results = $usrTlb->getVersionList($loginDetail['testAccount']);
        $view = new ViewModel(array(
            'results' => $results,
        ));
        $view->setTerminal(true);
        return $view;
 }
//Send push to user for app new update
 public function sendapppushAction(){
       $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
       $request = $this->getRequest();
       $loginDetails = $this->getLoggedInUserId();
       if($request->isPost()){
         $data = $request->getPost()->toArray();
  //Check version
       $latestVersion = $usrTlb->getVersion($data['platform'],$loginDetails['testAccount']);
      if (!empty($latestVersion['app_version'])) {
            $msg = "";
            $oldVersion = explode(".", $latestVersion['app_version']);
            $latestV = explode(".", $data['appversion']);
            $body =array();
            if ($latestV[0] > $oldVersion[0]){
                $body['appVerChange']   = "Major";
                $body['pushType']       = "majorPush";
                $body['appVersion']     = $data['appversion'];
                $body['expiry_date']    = "";
                $body['msg'] = "Dear User, You have an application update. Kindly update your App.";
                
            }else if ($latestV[0] == $oldVersion[0]){
            if ($latestV[1] > $oldVersion[1]) {
                //uncoment this code for minor push :-
//                $body['appVerChange']   = "Minor";
//                $body['pushType']       = "minorPush";
//                $body['appVersion']     = $data['appversion'];
//                $body['expiry_date']    = "";
//                $body['msg'] = "Dear User, You have an application update. Kindly update your App.";
                //comment this code
                $body['appVerChange']   = "Major";
                $body['pushType']       = "majorPush";
                $body['appVersion']     = $data['appversion'];
                $body['expiry_date']    = "";
                $body['msg'] = "Dear User, You have an application update. Kindly update your App.";
            }else if($latestV[1]== $oldVersion[1]){
                  if($latestV[2] > $oldVersion[2]){
//                $body['appVerChange']   = "Build";
//                $body['pushType']       = "minorPush";
//                $body['appVersion']     = $data['appversion'];
//                $body['expiry_date']    = "";
//                $body['msg'] = "Dear User, You have an application update. Kindly update your App.";
                $body['appVerChange']   = "Major";
                $body['pushType']       = "majorPush";
                $body['appVersion']     = $data['appversion'];
                $body['expiry_date']    = "";
                $body['msg'] = "Dear User, You have an application update. Kindly update your App.";    
//                      
          } 
         }
       }
       $json = json_encode($body,true);
       $array = array(
         "device_type"=>$data['platform'],
         'app_version'=>$data['appversion'],
         'expiry_date'=>"",
         'created_on'=> time(),
         'updated_on'=> time(),
         'testAccount'=>$loginDetails['testAccount']  
       );   
   //save version     
    $usrTlb->saveData('app_version',$array);
    $message1= "success";  
    $getUserList   = $usrTlb->getUsertoPush($data['platform'],$loginDetails['testAccount']);
   foreach($getUserList as $val){
    $message = array();   
    $payload = array();
    if($loginDetails['testAccount']==2){
    //now code to get total counter of user
    $userCounter = $usrTlb->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$val['username']."' and testAccount='".$loginDetails['testAccount']."'");
    if(!empty($userCounter)){
       $usrTlb->updateData('userNotifictionCount',array('totalApp'=>$userCounter[0]['totalApp']+1),array('userId'=>$val['username'],'testAccount'=>$loginDetails['testAccount'])); 
    }else{
       $usrTlb->saveData('userNotifictionCount',array('totalApp'=>1,'userId'=>$val['username'],'testAccount'=>$loginDetails['testAccount']));  
    }
    //now get user badge Count
    $userBadgeCount = $usrTlb->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$val['username']."' and testAccount='".$loginDetails['testAccount']."'");
    //now make json to send users
    $message['badge'] = (int)($userBadgeCount[0]['totalApp']+ $userBadgeCount[0]['totalChat']+ $userBadgeCount[0]['timelineCount']);
    
    }else{
     $message['badge'] = 1;   
    }
    //now make json to send users
    $message['mutable-content'] =1;
    $message['sound'] ="default";
    $message['chatId'] = "";
    $message['fromUser'] ="";
    $message['toUser'] = "";
    $message['alert'] = "testing";
    $message['bodyType'] = 'adminPush';
    $message['sender'] = "";
    $message['message']= $json;
    $message['timestamp'] = time();
    $payload['aps'] = $message;
    $payload['c'] = 1; 
 if(!empty($val["device_token"])){    
    if($val['device_type']=="iOS"){           
       $value = $this->sendapnspush($payload,$message1,$val["apnsToken"],'dev',$val['appType'],'alert'); 
       $value1= $this->sendapnspush($payload,$message1,$val["apnsToken"],'dist',$val['appType'],'alert');
     //echo "<pre>"; print_r($val); echo "<pre>"; echo "dev";echo "<pre>"; print_r($value); echo "<pre>"; print_r($value1); die("done");
    }else {
         $pushData = array("deviceId"=>$val["device_token"],
                            "message"=>$json,
                            'subject'=>"adminPush",
                            'pType'=>"adminPush"
                          );                    
         $value = $this->sendAndroidPush($pushData,$val['appType']);
         //echo "<pre>"; print_r($val); echo "<pre>"; print_r($value); die;
    }
   }   
  }
 }
 return $this->redirect()->toUrl('/users/appversion');  
}
  return new ViewModel(array());
}
//method for add notfication for single user
public function sendpushAction(){
  $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
  $loginDetails = $this->getLoggedInUserId();
  $request = $this->getRequest();
  $data = $request->getPost()->toArray();
  $data['save'] = 'send';
  $files = $request->getFiles()->toArray();
  //data store in notification data table 
  $notfiyData['title'] = $data['title'];
  if(($data['uiType']=='default') || ($data['uiType']=='web')){
      $html ='<font size="16">'.$data['msg'].'</font>';  
      $htmlMessage =$data['title'];   
   }else{
      $html = "";
      $htmlMessage = $data['msg'];   
   }
     $notfiyData['message'] =  $htmlMessage;
     $notfiyData['createdOn'] = time();
     $notfiyData['toSide'] =   "";
     $notfiyData['testAccount'] = $loginDetails['testAccount'];
     if($data['save']=='send'){
       $notfiyData['type'] =   "send";
     }else {
       $notfiyData['type'] =   "save";   
     }
     if(!empty($data['link'])){
       $notfiyData['link'] = $data['link'];
     }else{
       $notfiyData['link'] = "";   
     }
     if($data['uiType']=="web"){
       $notfiyData['uiType'] ="default";
     }else{
       $notfiyData['uiType'] = $data['uiType'];    
     } 
     $notfiyData['html'] = $html;
     $notificationDataId = $usrTlb->saveData('notificationData',$notfiyData);
    if(!empty($files['attachment'])){
       foreach($files['attachment'] as $val){
        //new code for file uploading
         $mime = $val['type'];
        if(strstr($mime, "video/")){
            //code for video upload
            $destination = "public/uploadedFiles/OstrichMedia/";
            $neoFileName = "notification/".uniqid('notif').'.mp4';
            $file = move_uploaded_file($val['tmp_name'],$destination.$neoFileName);
            if($file){
                $frame = $this->getVideoFrame($destination ,$neoFileName);
                $resizeImage110 = $this->reSizeImageforpost($destination, $frame, 720, 1280,15);
                $status = $this->putIntoS3Bucket($destination.$neoFileName,$neoFileName,2);
                $status1 = $this->putIntoS3Bucket($destination . $resizeImage110, 'small'.$frame,1);
                $status2 = $this->putIntoS3Bucket($destination . $frame, 'large'.$frame,1);
                if($status1 =='success' && $status2=='success' && $status =='success'){
                  $thumb   = $frame; 
                  unlink($destination . "/" . $frame);
                  unlink($destination . "/" . $resizeImage110);
                  unlink($destination.$neoFileName);
                  $usrTlb->saveData('notificationMedia',array('notificationDataId'=>$notificationDataId,'media'=>$neoFileName,'thumbNail'=>$thumb,'type'=>2,'creadtedBy'=>'admin','updatedBy'=>'admin','createdOn'=> time(),'updatedOn'=> time()));     
               }else{
                 unlink($destination . "/" . $frame);
                 unlink($destination . "/" . $resizeImage110);
                 unlink($destination.$neoFileName);  
              } 
           }
                         
     }else if(strstr($mime, "image/")){
             $image = "notification/".uniqid('notif').'.png';
             $success = $this->putIntoS3Bucket($val['tmp_name'],$image,1);
             if($success=="success"){
                $usrTlb->saveData('notificationMedia',array('notificationDataId'=>$notificationDataId,'media'=>$image,'thumbNail'=>"",'type'=>1,'creadtedBy'=>'admin','updatedBy'=>'admin','createdOn'=> time(),'updatedOn'=> time()));
             }
             
          }else{
            //do it later   
          }   
       } 
   }
  //if user send action only    
 if($data['save']=='send'){
      //getMediaDetail 
       $getMediaDetail = $usrTlb->getNotficationMedia($notificationDataId);
       $username = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6); 
       $interData = array();
       $interData['title']=$data['title'];
       $interData['updatedon']=time();
       //$interData['message']=$data['msg'];
       $interData['message'] = $htmlMessage;
       $interData['notificationDataId']=$notificationDataId;
       $interData['notifyId']=$username;
       $interData['type']="adminNotify";
       $interData['creatorId'] = 'admin';
       $interData1= $interData;
       $interData1['media'] = "";
       $interData1['bodyType'] = $interData['type'];
       $interData1['notificationMedia'] = $getMediaDetail;
       $interData1['chatAllowed'] = "";
       $interData1['nickname'] = "Admin";
       $interData1['eventName'] = "";
       $interData1['eventId'] = 0;
       if(!empty($data['link'])){
        $interData1['link'] = $data['link'];
       }else{
        $interData1['link'] = "";   
       }
       $interData1['html'] = $html;
       //"uiType": "default","full","salon"
       $interData1['uiType'] = $data['uiType'];
       $body = json_encode($interData1,true);
       $interData['body']=$body;
    //now code to get total counter of user
       $userCounter = $usrTlb->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".base64_decode($pera)."' and testAccount='".$loginDetails['testAccount']."'");
       if(!empty($userCounter)){
         $usrTlb->updateData('userNotifictionCount',array('totalApp'=>$userCounter[0]['totalApp']+1),array('userId'=>base64_decode($pera),'testAccount'=>$loginDetails['testAccount'])); 
       }else{
         $usrTlb->saveData('userNotifictionCount',array('totalApp'=>1,'userId'=>base64_decode($pera),'testAccount'=>$loginDetails['testAccount']));  
       }
       //now get user badge Count
      $userBadgeCount = $usrTlb->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".base64_decode($pera)."' and testAccount='".$loginDetails['testAccount']."'");
    //now make json to send users
     $message['badge'] = (int)($userBadgeCount[0]['totalApp']+ $userBadgeCount[0]['totalChat']+ $userBadgeCount[0]['timelineCount']);
    $message['mutable-content'] =1;
    $message['sound'] ="default";
    $message['chatId'] = "";
    $message['fromUser'] ="";
    $message['toUser'] = "";
    if($data['uiType']=='default'){
      $message['alert'] = array("title"=>"","body"=>$interData['message']);  
    }else{
      $message['alert'] = array("title"=>$data['title'],"body"=>$interData['message']);
    }
    $message['bodyType'] = $interData['type'];
    $message['sender'] = "";
    $message['message']= $body;
    $message['timestamp'] = time();
    $payload['aps'] = $message;
    $payload['c'] = 1;
    $message1= "success";
    $getUserList = $usrTlb->getDeviceDetail(base64_decode($data['username']),$loginDetails['testAccount']);
    foreach($getUserList as $list){
     $interData['username'] = $list['username'];
     $usrTlb->saveData('notification', $interData);
   if(!empty($list["device_token"])){  
      if($list['device_type']=="iOS"){ 
         $value = $this->sendapnspush($payload,$message1,$list["apnsToken"],'dev',$list['appType'],'alert'); 
         $value1= $this->sendapnspush($payload,$message1,$list["apnsToken"],'dist',$list['appType'],'alert'); 
        // echo "<pre>"; print_r($list); echo "<pre>"; echo "dev";echo "<pre>"; print_r($value); echo "<pre>"; print_r($value1); die("done");
      }else{
        $pushData["deviceId"] = $list["device_token"];
        $pushData["message"] = $body;
        $pushData["pType"] = 'app';
        $pushData["subject"] ='FCM push';
        $androidPush = $this->sendAndroidPush($pushData,$list['appType']);
        //echo "<pre>"; print_r($androidPush); die;
      }
     }
   }     
  }
  $send = array(
               'message' =>"Notification send successfully",
               'status'=>1  
              );
  return new JsonModel(array('send'=>$send)); 
}
 public function manageQuickPoleAction(){
   return new ViewModel(array());   
 }
 //method for add notfication
 public function addQuickPoleAction() {
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    //$loginDetail = $this->getLoggedInUserId();
    if ($request->isPost()) {
      $data = $request->getPost()->toArray();
     //data store in quick pole data table 
      $quick['question'] = $data['question'];
      if(!empty($data['option1'])){
       $quick['option1'] =  $data['option1'];
      }else{
       $quick['option1'] = "";   
      }
      if(!empty($data['option2'])){
       $quick['option2'] =  $data['option2'];
      }else{
       $quick['option2'] = "";   
      }
      if(!empty($data['option3'])){
       $quick['option3'] =  $data['option3'];
      }else{
       $quick['option3'] = "";   
      }
      if(!empty($data['option4'])){
       $quick['option4'] =  $data['option4'];
      }else{
       $quick['option4'] = "";   
      }
      if(!empty($data['option5'])){
       $quick['option5'] =  $data['option5'];
      }else{
       $quick['option5'] = "";   
      }
      if(!empty($data['option6'])){
       $quick['option6'] =  $data['option6'];
      }else{
       $quick['option6'] = "";   
      }
      $quick['group'] =   $data['usergroup'];
      $quick['timerValue'] =   $data['timer'];
      $quick['recordStatus'] =  "0";
      $quick['createdOn'] =   time();
      $quick['updatedOn'] =   time();
      $usrTlb->saveData('quickPole',$quick);
      $this->redirect()->toUrl("/users/manage-quick-pole");
     }
      return new ViewModel(array());
   }

     public function manageQuickPoleDataAction(){
      $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
      $loginDetail = $this->getLoggedInUserId();
      $request = $this->getRequest();
      $data = $request->getPost()->toArray(); 
      $paginator = $usrTlb->getQuickPoleData($data,true);
      $results = $this->paginationToArray($paginator,$data['page'],10);
      foreach ($results['allData'] as $key=>$value){
       $results['allData'][$key]['date'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['createdOn']),$loginDetail['timeZone'],2);
       } 
//        echo '<pre>'; print_r($results); die;
        $uri = $this->getRequest()->getUri();
        $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
            
        ));
        $view->setTerminal(true);
        return $view;
   }
   
   public function launchQuickPoleAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    //$usrTlb->updateData('quickPole',array('recordStatus'=>1),array('id'=> base64_decode($data['id'])));
    $quickPollDetail = $usrTlb->getDetail('quickPole','','id='.base64_decode($data['id']));
    $getUserGroup = $usrTlb->getUserListforQuickPole($quickPollDetail[0]['group'],2);
   echo "<pre>";  print_r($getUserGroup); die;
    if(!empty($getUserGroup)){ 
      foreach($getUserGroup as  $list ){
       $config = $this->getServiceLocator()->get('config');
       $link = $config['settings']['quickPole'].'quickPoll/'.base64_encode($list['userId']);
       $username = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
       $interData = array();
       $interData['title']="New Quick Poll";
       $interData['updatedon']=time();
       $interData['message']="Here is New Quick Poll for You.Let share your opinion";
       $interData['notificationDataId']=0;
       $interData['notifyId']=$username;
       $interData['type']="quickPoll";
       $interData['creatorId'] = "admin";
       $interData['eventId'] = base64_decode($data['id']);
       $interData1= $interData;
       $interData1['link'] =$link;
       $interData1['bodyType'] = $interData['type'];
       $interData1['userId'] = "admin";
       $interData1['nickname'] = "Admin";
       $body = json_encode($interData1,true);
       $interData['body']=$body;
       //now make json to send users
       $message['badge'] = 1;
       $message['content-available'] =1;
       $message['bodyType'] = $interData['type'];
       $message['sender'] = "";
       $message['message']= $body;
       $message['timestamp'] = time();
       $interData['username'] = $list['userId'];
       $usrTlb->saveData('notification', $interData);
       $usrTlb->saveData('assignQuickPoll',array('userId'=>$list['userId'],'questionId'=>base64_decode($data['id']),'createdOn'=> time(),'updatedOn'=> time(),'status'=>1));
       $text =  json_encode($message);
       $notificationId = $this->getToken(20);
       $eventRequest = array("type"=>"genericAndroidEvent","eventId"=>base64_decode($data['id']),"notificationId"=>$notificationId,"token"=>"123456",'userList'=>$list['userId'],'bodyType'=>'quickPoll','creatorId'=>'admin','eventName'=>'','text'=>$text);       
       $this->sendXmPush($eventRequest); 
      } 
    }
   return new JsonModel(array('message'=>"success"));
 }
   
    public function convertlisttoexcelForPracAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $action = $this->params()->fromroute('id', NULL);
    $practitionerData= $usrTlb->bookedPracClassData($action); 
    $DetailsValue = array();
     foreach ($practitionerData as $key => $val) {
     $DetailsValue[] = $val;
          }
          $objPHPExcel = new \PHPExcel();
          $default_border = array('style' => \PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '1006A3'));
          $style_header = array('borders' => array('bottom' => $default_border,'left' => $default_border,'top' => $default_border,'right' => $default_border,),
                  'fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'E1E0F7'),),'font' => array('bold' => false,'font-family' => 'Arial','size' => '10'));
          $objPHPExcel->getProperties()->setTitle("Practitioner Master Class in Excel");
          $objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A1', 'Sr.No')
                  ->setCellValue('B1', 'Master Class')
                  ->setCellValue('C1', 'FirstName')
                  ->setCellValue('D1', 'LastName')
                  ->setCellValue('E1', 'Email')
                  ->setCellValue('F1', 'Phone')
                  ->setCellValue('G1', 'CompanyName')
                  ->setCellValue('H1', 'Designation');                  

     $itemcount = count($DetailsValue);        
 for ($i = 0; $i < $itemcount; $i++) 
 {           
     $j = $i + 2;           
     $objPHPExcel->setActiveSheetIndex(0)                    
             ->setCellValue('A' . $j, $i+1)                    
             ->setCellValue('B' . $j, $DetailsValue[$i]['title'])                   
             ->setCellValue('C' . $j, $DetailsValue[$i]['firstName']) 
             ->setCellValue('D'. $j, $DetailsValue[$i]['lastName']) 
             ->setCellValue('E'. $j, $DetailsValue[$i]['email']) 
             ->setCellValue('F'. $j, $DetailsValue[$i]['phone'])
             ->setCellValue('G'. $j, $DetailsValue[$i]['companyName']) 
             ->setCellValue('H'. $j, $DetailsValue[$i]['designation']);
             
 }
 $objPHPExcel->getActiveSheet()->setTitle('MasterClass List');       
 $objPHPExcel->getActiveSheet()->setShowGridLines(true);       
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, charset=utf-8');     
 header('Content-Transfer-Encoding: binary');     
 header('Content-Disposition: attachment; filename="PractitionerMasterClassExport.xlsx"');      
 header('Cache-Control: max-age=0');       
 $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');        
 $objWriter->save('php://output');       
 exit;  
    
 }
 public function convertlisttoexcelForVipAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $action = $this->params()->fromroute('id', NULL);
    $practitionerData= $usrTlb->getBookedVipMasterClassForExcel($action); 
    $DetailsValue = array();
     foreach ($practitionerData as $key => $val) {
     $DetailsValue[] = $val;
//    echo '<pre>'; print_r($DetailsValue); die(' ol');
          }
          $objPHPExcel = new \PHPExcel();
          $default_border = array('style' => \PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '1006A3'));
          $style_header = array('borders' => array('bottom' => $default_border,'left' => $default_border,'top' => $default_border,'right' => $default_border,),
                  'fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'E1E0F7'),),'font' => array('bold' => false,'font-family' => 'Arial','size' => '10'));
          $objPHPExcel->getProperties()->setTitle("Vip Master Class in Excel");
          $objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A1', 'Sr.No')
                  ->setCellValue('B1', 'Master Class')
                  ->setCellValue('C1', 'FirstName')
                  ->setCellValue('D1', 'LastName')
                  ->setCellValue('E1', 'Email')
                  ->setCellValue('F1', 'Phone')
                  ->setCellValue('G1', 'CompanyName')
                  ->setCellValue('H1', 'Designation');
                  

     $itemcount = count($DetailsValue);        
 for ($i = 0; $i < $itemcount; $i++) 
 {           
     $j = $i + 2;           
     $objPHPExcel->setActiveSheetIndex(0)                    
             ->setCellValue('A' . $j, $i+1)                    
             ->setCellValue('B' . $j, $DetailsValue[$i]['title'])                   
             ->setCellValue('C' . $j, $DetailsValue[$i]['firstName']) 
             ->setCellValue('D'. $j, $DetailsValue[$i]['lastName']) 
             ->setCellValue('E'. $j, $DetailsValue[$i]['email']) 
             ->setCellValue('F'. $j, $DetailsValue[$i]['phone'])
             ->setCellValue('G'. $j, $DetailsValue[$i]['companyName']) 
             ->setCellValue('H'. $j, $DetailsValue[$i]['designation']); 
             
 }
 $objPHPExcel->getActiveSheet()->setTitle('VipBookedMasterClass');       
 $objPHPExcel->getActiveSheet()->setShowGridLines(true);       
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, charset=utf-8');     
 header('Content-Transfer-Encoding: binary');     
 header('Content-Disposition: attachment; filename="VipBookedMasterClass.xlsx"');      
 header('Cache-Control: max-age=0');       
 $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');        
 $objWriter->save('php://output');       
 exit;  
    
 }
public function converttoexcelForBookingReqAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $action = $this->params()->fromroute('id', NULL);
    $practitionerData= $usrTlb->reqForBookingMasterClassForExcel($action); 
     foreach($practitionerData as $key=>$value){
        $practitionerData[$key]['date'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['createdOn']),$loginDetail['timeZone'],1);
    }
 
    $DetailsValue = array();
     foreach ($practitionerData as $key => $val) {
     $DetailsValue[] = $val;
        //echo '<pre>'; print_r($DetailsValue); die;
     
          }
          $objPHPExcel = new \PHPExcel();
          $default_border = array('style' => \PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '1006A3'));
          $style_header = array('borders' => array('bottom' => $default_border,'left' => $default_border,'top' => $default_border,'right' => $default_border,),
                  'fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'E1E0F7'),),'font' => array('bold' => false,'font-family' => 'Arial','size' => '10'));
          $objPHPExcel->getProperties()->setTitle("MasterCLassBookingRequest in Excel");
          $objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A1', 'Sr.No')
                  ->setCellValue('B1', 'Date')
                  ->setCellValue('C1', 'Master Class')
                  ->setCellValue('D1', 'Type')
                  ->setCellValue('E1', 'FirstName')
                  ->setCellValue('F1', 'LastName')
                  ->setCellValue('G1', 'Email')
                  ->setCellValue('H1', 'Phone')
                  ->setCellValue('I1', 'CompanyName')
                  ->setCellValue('J1', 'Designation');


     $itemcount = count($DetailsValue);        
 for ($i = 0; $i < $itemcount; $i++) 
 {           
     $j = $i + 2;           
     $objPHPExcel->setActiveSheetIndex(0)                    
             ->setCellValue('A' . $j, $i+1)  
             ->setCellValue('B' . $j, $DetailsValue[$i]['date'])                   
             ->setCellValue('C' . $j, $DetailsValue[$i]['title'])
             ->setCellValue('D' . $j, $DetailsValue[$i]['type'])                   
             ->setCellValue('E' . $j, $DetailsValue[$i]['firstName']) 
             ->setCellValue('F'. $j, $DetailsValue[$i]['lastName']) 
             ->setCellValue('G'. $j, $DetailsValue[$i]['email']) 
             ->setCellValue('H'. $j, $DetailsValue[$i]['phone'])
             ->setCellValue('I'. $j, $DetailsValue[$i]['companyName']) 
             ->setCellValue('J'. $j, $DetailsValue[$i]['designation']); 
             
   }
 $objPHPExcel->getActiveSheet()->setTitle('BookingRequest');       
 $objPHPExcel->getActiveSheet()->setShowGridLines(true);       
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, charset=utf-8');     
 header('Content-Transfer-Encoding: binary');     
 header('Content-Disposition: attachment; filename="BookingRequest.xlsx"');      
 header('Cache-Control: max-age=0');       
 $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');        
 $objWriter->save('php://output');       
 exit;  
    
 }

 public function manageLeaderBoardAction(){
   return new ViewModel(array());   
 } 
 public function leaderBoardDataAction(){
      $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
      $loginDetail = $this->getLoggedInUserId();
      $request = $this->getRequest();
      $data = $request->getPost()->toArray(); 
      $paginator = $usrTlb->leaderBoardData($data,true);
      $results = $this->paginationToArray($paginator,$data['page'],10);
//        echo '<pre>'; print_r($results); die;
     
        $uri = $this->getRequest()->getUri();
        $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
            
        ));
        $view->setTerminal(true);
        return $view;
   }
 public function addCelebrityAction(){
   $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    //$loginDetail = $this->getLoggedInUserId();
    if ($request->isPost()) {
      $data = $request->getPost()->toArray();
//      $url = "https://chat.techhrconf.com/photoGrid/celebStatus/?status=1&imageUrl=".$data['url'];
      $url = "https://chat.techhrconf.com/photoGrid/celebStatus/?status=".$data['type']."&imageUrl=".$data['url']."";
     $ch = curl_init($url);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
     $curl_scraped_page = curl_exec($ch); 
     curl_close($ch);
     //data store in quick pole data table 
      $this->redirect()->toUrl("/users/add-celebrity");
    }
   return new ViewModel(array());
 }
 //die
 public function photoGridAction(){
   $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $request = $this->getRequest();
   $getMedia = $usrTlb->getPhotoGridMedia();
   $mediaUrl=$this->getMediaUrl();
     $view = new ViewModel(array(
         'mediaUrl'=>$mediaUrl,
         'getMedia'=>$getMedia
        ));
        $view->setTerminal(true);
        return $view;  
    } 
 public function snapAction(){
     
   $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $view = new ViewModel(array(
        
    ));
    $view->setTerminal(true);
    return $view;  
 }
 public function snapfileAction(){
    $file = $_FILES;
    $im = imagecreatefrompng('original.png');
    $size = min(imagesx($im), imagesy($im));
    $im2 = imagecrop($im, ['x' => 1080, 'y' => 35, 'width' => 320, 'height' => 960]);
if ($im2 !== FALSE) {
    imagepng($im2, 'example-cropped.png');
    imagedestroy($im2);
}
imagedestroy($im);  
 }
 //method for manage story
 public function storyAction(){
   $viewModel = new ViewModel();
   return $viewModel;      
  } 
  public function getStoryDataAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $loginDetail = $this->getLoggedInUserId();
    $paginator = $usrTlb->getAllStory($data,true,$loginDetail['testAccount']);
    $results = $this->paginationToArray($paginator,$data['page'],20);
    foreach($results['allData'] as $key=>$value){
        $results['allData'][$key]['date'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['createdOn']),$loginDetail['timeZone'],1);
    }
    $uri = $this->getRequest()->getUri();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results
       ));
        $view->setTerminal(true);
        return $view;
   }  
  public function storyMediaAction(){
   $pera = $this->params()->fromroute('id', NULL);
   $viewModel = new ViewModel(array('pera'=>$pera));
   return $viewModel;      
   }

 public function viewStoryMediaAction(){
      $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
      $request = $this->getRequest();
      $data = $request->getPost()->toArray();
      $data['socialPostId']= base64_decode($data['pera']);
      $paginator = $usrTlb->getStoryMedia($data,true);
      $results = $this->paginationToArray($paginator,$data['page'],20);
      $loginDetail = $this->getLoggedInUserId();
      foreach($results['allData'] as $key=>$value){
      $results['allData'][$key]['date'] = $this->getUserTimeZone($value['timeStamp'],$loginDetail['timeZone'],1);
      }
    $mediaUrl=$this->getMediaUrl();
    $uri = $this->getRequest()->getUri();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
             'mediaUrl'=>$mediaUrl
      ));
    $view->setTerminal(true);
    return $view;    
 }
 public function deleteStoryAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $usrTlb->updateData('story',array('status'=>4),array('id'=> base64_decode($data['id'])));
    return new JsonModel(array('message'=>"success"));
 }
//method for user notification list
 public function usernotificationAction(){
   $pera = $this->params()->fromroute('id', NULL);
   $viewModel = new ViewModel(array('pera'=>$pera));
   return $viewModel;      
   }
//method for user notification list data
 public function usernotificationDataAction(){
      $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable');
      $request = $this->getRequest();
      $data = $request->getPost()->toArray();
      $idArray= explode("-",base64_decode($data['pera']));
      $data['notificationId'] = $idArray[0];
      $data['action'] = $idArray[1];
      $paginator = $usrTlb->getNotificiationUsers($data,true);
      $results = $this->paginationToArray($paginator,$data['page'],5);
      $loginDetail = $this->getLoggedInUserId();
      foreach($results['allData'] as $key=>$value){
       $results['allData'][$key]['updatedon'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['updatedon']),$loginDetail['timeZone'],3);
       if(!empty($value['viewUpdatedOn'])){
           $results['allData'][$key]['viewUpdatedOn'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['viewUpdatedOn']),$loginDetail['timeZone'],3);
       }else{
           $results['allData'][$key]['viewUpdatedOn'] = "";
       }
     }
      $view = new ViewModel(array(
            'paginator' => $paginator,
            'results' => $results,
            'action'=>$data['action']
      ));
    $view->setTerminal(true);
    return $view;    
 }
  public function deleteNotificationAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
//    echo '<pre>'; print_r($data); die;
     $usrTlb->updateData('notificationData',array('status'=>0),array('id'=>$data['id']));
     $usrTlb->deleteUserData('notification',array('notificationDataId'=> $data['id']));   
    return new JsonModel(array('message'=>"success"));
 }
 //method for add notfication
 public function addSessionNotificationAction() {
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $loginDetail = $this->getLoggedInUserId();
    if($request->isPost()) {
      $data = $request->getPost()->toArray();
     //data store in notification data table 
      $notfiyData['title'] = $data['title'];
      $notfiyData['message'] =  $data['msg'];
      $notfiyData['createdOn'] = time();
      $notfiyData['toSide'] =   $data['usergroup'];
      $notfiyData['testAccount'] = $loginDetail['testAccount'];
      if($data['save']=='send'){
       $notfiyData['type'] =   "send";
      }else {
       $notfiyData['type'] =   "save";   
      }
      if(!empty($data['link'])){
       $notfiyData['link'] = $data['link'];
      }else{
       $notfiyData['link'] = "";   
      }
      $notfiyData['uiType'] = $data['uiType'];
      $notificationDataId = $usrTlb->saveData('notificationData',$notfiyData);
     //save file of notfication sol media  table 
     $files = $request->getFiles()->toArray();
    if(!empty($files['attachment'])){
      foreach($files['attachment'] as $val){
        //new code for file uploading
         $mime = $val['type'];
        if(strstr($mime, "video/")){
            //code for video upload
            $destination = "public/uploadedFiles/OstrichMedia/";
            $neoFileName = "notification/".uniqid('notif').'.mp4';
            $file = move_uploaded_file($val['tmp_name'],$destination.$neoFileName);
            if($file){
                $frame = $this->getVideoFrame($destination ,$neoFileName);
                $resizeImage110 = $this->reSizeImageforpost($destination, $frame, 720, 1280,15);
                $status = $this->putIntoS3Bucket($destination.$neoFileName,$neoFileName,2);
                $status1 = $this->putIntoS3Bucket($destination . $resizeImage110, 'small'.$frame,1);
                $status2 = $this->putIntoS3Bucket($destination . $frame, 'large'.$frame,1);
                if($status1 =='success' && $status2=='success' && $status =='success'){
                  $thumb   = $frame; 
                  unlink($destination . "/" . $frame);
                  unlink($destination . "/" . $resizeImage110);
                  unlink($destination.$neoFileName);
                  $usrTlb->saveData('notificationMedia',array('notificationDataId'=>$notificationDataId,'media'=>$neoFileName,'thumbNail'=>$thumb,'type'=>2,'creadtedBy'=>'admin','updatedBy'=>'admin','createdOn'=> time(),'updatedOn'=> time()));     
               }else{
                 unlink($destination . "/" . $frame);
                 unlink($destination . "/" . $resizeImage110);
                 unlink($destination.$neoFileName);  
               } 
          }
                         
     }else if(strstr($mime, "image/")){
             $image = "notification/".uniqid('notif').'.png';
             $success = $this->putIntoS3Bucket($val['tmp_name'],$image,1);
             if($success=="success"){
                $usrTlb->saveData('notificationMedia',array('notificationDataId'=>$notificationDataId,'media'=>$image,'thumbNail'=>"",'type'=>1,'creadtedBy'=>'admin','updatedBy'=>'admin','createdOn'=> time(),'updatedOn'=> time()));
             }
             
          }else{
            //do it later   
          }   
       } 
   }
   if(!empty($data['link'])){
        $link = $data['link'];
     }else{
        $link = "";   
     }
   //code for job que
   $config = $this->getServiceLocator()->get('config');    
   $jobData['url'] = $config['settings']['JobQueUrl'].'/application/index/sendnotificationpush';
   $jobData['jobClass'] = 'JobQueue';
   $jobData['queue'] = 'SpecialJobQ';
   $jobData['time'] = time();
   $jobData['notificationDataId'] = $notificationDataId;
   $jobData['testAccount'] = $loginDetail['testAccount'];
   $jobData['uiType'] = $data['uiType'];
   $jobData['link'] = $link;
   $this->insertJobQueue($jobData);
   $this->redirect()->toUrl("/users/notification");
   }
   return new ViewModel(array());
 }
//method for manage meeting
public function manageMeetingAction(){ 
   $viewModel = new ViewModel();
   return $viewModel;      
 }
//method for meeting data 
  public function getMeetingDataAction(){
   $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $request = $this->getRequest();
   $data = $request->getPost()->toArray();
   $loginDetail = $this->getLoggedInUserId();
   $paginator = $usrTlb->getAllMeeting($data,true,$loginDetail['testAccount']);
   $results = $this->paginationToArray($paginator,$data['page'],20);
   foreach($results['allData'] as $key=>$value){
      $results['allData'][$key]['createdOn'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['createdOn']),$loginDetail['timeZone'],3);
      $results['allData'][$key]['dateTime'] = $this->getUserTimeZone(date("Y-m-d H:i:s",$value['dateTime']),$loginDetail['timeZone'],3);
      $results['allData'][$key]['media'] = $usrTlb->getMeetingMedia($value['meetingId']);
   }
   $mediaUrl=$this->getMediaUrl();
 //  echo "<pre>"; print_r($results); die;
   $uri = $this->getRequest()->getUri();
   $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
            'mediaUrl'=>$mediaUrl
       ));
   $view->setTerminal(true);
   return $view;
 } 
}