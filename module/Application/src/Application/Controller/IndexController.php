<?php
namespace Application\Controller;
use CustomLib\Controller\CustomController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
//use Zend\Filter\StripTags;
//use Zend\Filter\StringTrim;
use Zend\View\Model\JsonModel;
use CustomLib\Service\UserPassword;
class IndexController extends CustomController
{ 
//Admin home page   
 public function indexAction(){
   $eventTable = $this->getServiceLocator()->get('Application\Model\EventTable');
   $loginDetails = $this->getLoggedInUserId();
   $result = $eventTable->getDasBoardCounter($loginDetails['testAccount']);
   //code for booth counter api
   $boothList = $eventTable->getDetail('booth',array('id','boothName'),"status=1");
   //first code User visited booth wise
   $userVisitedBoothWise = array();
   foreach($boothList as $value1){
      $booth['boothName'] = $value1['boothName'];
      $booth['boothCounter'] = $eventTable->getProfileCounter('boothHitLogs',"boothId='".$value1['id']."' and status=1 and type=0");
      $userVisitedBoothWise[] = $booth;
   }
   //second code Attendees connected booth wise
   $attendesConnectedBoothWise = array();
   foreach($boothList as $value2){
      $booth1['boothName'] = $value2['boothName'];
      $booth1['boothCounter'] = $eventTable->getProfileCounter('boothHitLogs',"boothId='".$value2['id']."' and status=1 and type=1");
      $attendesConnectedBoothWise[] = $booth1;
   }
   //third code for total booth visited today
   $todayDate = date("Y-m-d");
   $totaluservisitedboothtoday = $eventTable->getProfileCounter('boothHitLogs',"DATE_FORMAT(FROM_UNIXTIME(t1.createdOn), '%Y-%m-%d')='".$todayDate."' and status=1 and type=0");
   $totaluservisitedbooth = $eventTable->getProfileCounter('boothHitLogs',"status=1 and type=0"); 
   //now code for login section
   $totalLogin = $eventTable->getProfileCounter('users',"active=1 and status=0 and testAccount=3 and fb_verfiy=1",0);
   $todayDateLogin= $eventTable->getProfileCounter('users',"active=1 and status=0 and testAccount=3 and fb_verfiy=1 and DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d')='".$todayDate."'",0);
   //now code for registration
   $todayDateRegistrtion = $eventTable->getProfileCounter('users',"active=1 and status=0 and testAccount=3 and  DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d')='".$todayDate."'",0);
   //to get week range
    $monday = strtotime("last monday");
    $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
    $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
    $postData['startingDayOfWeek'] = date("Y-m-d",$monday);
    $postData['lastDayOfWeek'] = date("Y-m-d",$sunday);
     $totalRegistrtion = $eventTable->getProfileCounter('users',"active=1 and status=0 and testAccount=3 and  DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d') >= '".$postData['startingDayOfWeek']."' and DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d')<='".$postData['lastDayOfWeek']."'",0);
      //now code for weekwise login pie chart
     $textdt= date("Y-m-01");
     $currdt= strtotime( $textdt);
     $nextmonth=strtotime($textdt."+1 month");
     $i=0;
     $flag=true;
     $weekData = [];
    do 
    {
        $weekday= date("w",$currdt);
        $endday=abs($weekday-7);
        $startarr[$i]=$currdt;
        $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
        $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");
            if($endarr[$i]>=$nextmonth)
            {
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");
                    $flag=false;		
            }
            //echo "Week ".($i+1). "-> start date = ". date("Y-m-d",$startarr[$i])." end date = ". date("Y-m-d",$endarr[$i])."<br>";
            $weekData[($i+1)]['startDate'] = date("Y-m-d",$startarr[$i]);
            $weekData[($i+1)]['endDate'] = date("Y-m-d",$endarr[$i]);
            //$weekArray[] = $weekData;
        $i++;

    }while($flag);
      $loginWeekData = $weekData;
    //now code week wise registrtion
    foreach($weekData as $key=>$day){
       $weekTotalLogin = $eventTable->getProfileCounter('users',"active=1 and status=0 and testAccount=3 and  DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d') >= '".$day['startDate']."' and DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d')<='".$day['endDate']."'",0);
       $weekData[$key]['totalCounter'] = $weekTotalLogin;
       if($key==1){
        $weekData[$key]['color'] = '#dc3912';
        $weekData[$key]['weekName']= "First Week";
       }elseif($key==2){
        $weekData[$key]['color'] = '#ff9900';
        $weekData[$key]['weekName']= "Second Week";
       }
       elseif($key==3){
       $weekData[$key]['color'] = '#109618';
       $weekData[$key]['weekName']= "Third Week";
       }
       elseif($key==4){
       $weekData[$key]['color'] = '#990099';
       $weekData[$key]['weekName']= "Fourth Week";
       }else{
       $weekData[$key]['color'] = '#3366cc';
       $weekData[$key]['weekName']= "Fifth Week";
       }
    }
    //now code week wise login
    foreach($loginWeekData as $key=>$day){
       $weekTotalLogin = $eventTable->getProfileCounter('users',"active=1 and status=0 and testAccount=3 and fb_verfiy=1 and  DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d') >= '".$day['startDate']."' and DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d')<='".$day['endDate']."'",0);
       $loginWeekData[$key]['totalCounter'] = $weekTotalLogin;
       if($key==1){
        $loginWeekData[$key]['color'] = '#dc3912';
        $loginWeekData[$key]['weekName']= "First Week";
       }elseif($key==2){
        $loginWeekData[$key]['color'] = '#ff9900';
        $loginWeekData[$key]['weekName']= "Second Week";
       }
       elseif($key==3){
       $loginWeekData[$key]['color'] = '#109618';
       $loginWeekData[$key]['weekName']= "Third Week";
       }
       elseif($key==4){
       $loginWeekData[$key]['color'] = '#990099';
       $loginWeekData[$key]['weekName']= "Fourth Week";
       }else{
       $loginWeekData[$key]['color'] = '#3366cc';
       $loginWeekData[$key]['weekName']= "Fifth Week";
       }
    }
   return new ViewModel(array('result'=>$result,'userVisitedBoothWise'=>$userVisitedBoothWise,'attendesConnectedBoothWise'=>$attendesConnectedBoothWise,'totaluservisitedboothtoday'=>$totaluservisitedboothtoday,'totaluservisitedbooth'=>$totaluservisitedbooth,'totalLogin'=>$totalLogin,'todayDateLogin'=>$todayDateLogin,'todayDateRegistrtion'=>$todayDateRegistrtion,'totalRegistrtion'=>$totalRegistrtion,'weekData'=>$weekData,'loginWeekData'=>$loginWeekData));
 }  
//login action for Super Admin
public function loginAction(){
   $request = $this->getRequest();
   $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $passUtil = new UserPassword();
   if($request->isPost()){
       $data = $request->getPost()->toArray();
       $passUtil = new UserPassword();
       $usrPass = $passUtil->create($data['password']);
   
       $userData = $userTable->getData('super_admin',array('where' => array('app_username' => $data['username'],'password' => $usrPass,'status' => '1')));    
//     echo '<pre>';    print_r($usrPass); die(' lol'); 

       if(!empty($userData)){
          $sess = new Container('User');
          $sess->offsetSet('userId',$userData[0]['id']);
          $sess->offsetSet('appName',$userData[0]['appName']);
          $sess->offsetSet('username',$userData[0]['username']);
          $sess->offsetSet('user_type',$userData[0]['user_type']);
         
          if($userData[0]['app_username']=="Edbrand"){
            $sess->offsetSet('testAccount',3);
            return $this->redirect()->toUrl('/eventprogram/manage-user'); 
          }if($userData[0]['user_type']=="3"){
//            $sess->offsetSet('user_type',3);
            return $this->redirect()->toUrl('eventprogram/manage-user'); 
          }
          else{
           $this->flashMessenger()->addErrorMessage($this->translate("some thing went wrong"));
          }
          
      }else{
          $this->flashMessenger()->addErrorMessage($this->translate("Wrong Username or password."));
      }
    }
   $viewModel = new ViewModel();
   $viewModel->setTerminal(true);
   return $viewModel;
}
//User logout action 
 public function logoutAction(){
        $sess = new Container('User');
        $sess->getManager()->destroy();
        return $this->redirect()->toUrl('/');
 }
 //Set user time zone in session 
 public function settimezoneAction(){
       $request = $this->getRequest();
       $data = $request->getPost()->toArray();
       $sess = new Container('User');
       $sess->offsetSet('timeZone',$data['timezone']);
       return new JsonModel(array('message'=>"success"));
  }  
}