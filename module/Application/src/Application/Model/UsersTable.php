<?php
namespace Application\Model;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Select;
use CustomLib\Model\CustomTable;
use Zend\Db\Sql\Predicate\Expression;

class UsersTable extends CustomTable
{
 protected $table = 'users';
 
 
 
 
 public function getAllVideos($data=array(), $pageing = true,$testAccount)
    {
        $searchUser = strtolower($data['search']);  
        //search
        if ($searchUser != '') {
            $searchnm = "and ((lower(t1.mediaName) like \"%" . $searchUser . "%\"))";
        } else {
            $searchnm = '';
        }
        
         if(empty($data['sectionFilter'])){
         $sectionFilter = "";
     }else{
         $sectionFilter = "and t1.sectionType='".$data['sectionFilter']."'";  
     }
        
     
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'videos'
                ));
        $select->columns(array(
          'id',
         'media',
         'mediaName',   
         'thumbNail',
         'type',
         'createdOn',
            'sectionType',
            'status'
         
      ));
//      $select->where(array("t1.testAccount='".$testAccount."' and t1.status !=3 and t1.sectionType='".$data['type']."'  $searchnm   "));
      $select->where(array("t1.testAccount='".$testAccount."' and t1.status !=3   $searchnm  $sectionFilter "));
      
      $select->order('t1.id desc');
//      $smt = $sql->prepareStatementForSqlObject($select);
//      echo $smt->getSql(); die;
        if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    }
 
 
  public function getAllBooth($data=array(), $pageing = true,$testAccount)
    {
        $searchUser = strtolower($data['search']);  
        //search
        if ($searchUser != '') {
            $searchnm = "and ((lower(t1.boothName) like \"%" . $searchUser . "%\"))";
        } else {
            $searchnm = '';
        }
     
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'booth'
                ));
        $select->columns(array(
          'id',
         'boothName',
         'boothorder',
          'email',  
         'status',
          'name',  
         'createdOn',
         
      ));
        
     $select->join(
        array('t2' =>'super_admin'),
        new Expression('t2.username = t1.userId'),
        array(
       'username'
       ),
        'LEFT'
    );   
        
      $select->where(array("t1.testAccount='".$testAccount."' and t1.status !=3  $searchnm   "));
      $select->order('t1.boothorder ASC');
    //  $smt = $sql->prepareStatementForSqlObject($select);
     // echo $smt->getSql(); die;
        if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    }
    
    
    
    public function getExperienceZoneMedia($data=array(), $pageing = true,$testAccount)
    {
        $searchUser = strtolower($data['search']);  
        //search
        if ($searchUser != '') {
            $searchnm = "and ((lower(t1.mediaName) like \"%" . $searchUser . "%\"))";
        } else {
            $searchnm = '';
        }
     
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'experienceZone'
                ));
        $select->columns(array(
          'id',
         'media',
         'mediaName',
         'thumbNail',
         'type',
         'createdOn',
            'sectionType',
            'status'
         
      ));
      $select->where(array("t1.testAccount='".$testAccount."' and t1.status !=3 and t1.sectionType='experienceZone'  $searchnm   "));
      $select->order('t1.id desc');
    //  $smt = $sql->prepareStatementForSqlObject($select);
     // echo $smt->getSql(); die;
        if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    }
 
    
     public function getBoothMedia($data=array(), $pageing = true,$testAccount,$type)
    {
        $searchUser = strtolower($data['search']);  
        //search
        if ($searchUser != '') {
            $searchnm = "and ((lower(t1.mediaName) like \"%" . $searchUser . "%\"))";
        } else {
            $searchnm = '';
        }
     
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'boothMedia'
                ));
        $select->columns(array(
          'id',
          'boothId',  
         'media',
         'mediaName',
         'thumbNail',
         'type',
         'createdOn',
          'status'
         
      ));
        if($type=="1"){
      $select->where(array("t1.testAccount='".$testAccount."' and t1.boothMediaType=0 and t1.status !=3 and t1.boothId='".$data['pera']."'  $searchnm   "));
        }else{
      $select->where(array("t1.testAccount='".$testAccount."' and t1.boothMediaType=1  and t1.status !=3 and t1.boothId='".$data['pera']."'  $searchnm   "));
            
        }   
      
      $select->order('t1.id desc');
    //  $smt = $sql->prepareStatementForSqlObject($select);
     // echo $smt->getSql(); die;
        if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    }
    
    
    
   public function getUserName($userId)
    {
 
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'booth'
                ));
        $select->columns(array(
          'name',
      ));
      $select->where(array("t1.testAccount='3'  and t1.userId='".$userId."'  "));
      $smt = $sql->prepareStatementForSqlObject($select);
//      echo $smt->getSql(); die;
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        
    } 
    
    
      public function getAllAttendeForExcel($action){
        $sql = new Sql($this->getAdapter());
            $select = new Select();
            $select->from(array(
                't1' => 'users'
            ));
            $select->columns(array(
            'username',
         'uts',
         'type',
         'attendeType',
         'qrCode',
         'sms_code'    
        )); 
    //
$select->join(
        array('t2' =>'vcard_search'),
        new Expression('t2.username = t1.username'),
        array(
       'countryCode',
       'firstName',
       'lastName',
       'email',
       'phone',
       'city',
       'country',
       'companyName',
       'designation',
       ),
        'LEFT'
    );

    $select->join(array('t3' =>'auth_session'), new Expression('t3.user_id = t1.username'), array('device_type'),'LEFT');
    $select->join(array('t4' =>'last'), new Expression('t4.username = t1.username'), array('seconds'),'LEFT');    
        
    $select->order(array(
          't1.id DESC'
     ));
      
    $select->where(array("t1.extra3=1 and t1.active=1 and t1.status =0 and t1.attendeType!='TechHR' and t1.testAccount='3'"));

      $smt = $sql->prepareStatementForSqlObject($select);
      $result = $this->resultSetPrototype->initialize($smt->execute())
      ->toArray();
      return $result;
    }
    //
    
    
     public function getBoothOfUser($data=array(),$type)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'booth'
                ));
        $select->columns(array(
          'id',
          'boothName',  
           'description',
           'title'
      ));
        if($type=="1"){
      $select->where(array("t1.testAccount='3'  and t1.userId='".$data['username']."'  "));
        }else{
      $select->where(array("t1.testAccount='3'  and t1.id='".$data."'  "));
            
        }
      $select->order('t1.id desc');
      $smt = $sql->prepareStatementForSqlObject($select);
//      echo $smt->getSql(); die;
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        
    } 
   
    
    
 
}




