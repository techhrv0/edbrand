<?php
namespace Application\Model;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Select;
use CustomLib\Model\CustomTable;
use Zend\Db\Sql\Predicate\Expression;

class EventTable extends CustomTable
{
    protected $table = 'eventProgramForAu';
    
    
  
    
    
    //method to get list of guests
    public function getAllUsers($data=array(), $pageing = true, $testAccount)
    {
       $searchUser = strtolower($data['search']);
        //search
        if ($searchUser != '') {
           $searchnm = "and ((lower(t2.firstName) like \"%" . $searchUser . "%\") or (lower(t2.lastName) like \"%" . $searchUser . "%\") or (lower(t2.email) like \"%" . $searchUser . "%\") or (lower(t2.phone) like \"%" . $searchUser . "%\") or (lower(t3.role) like \"%" . $searchUser . "%\"))";
        } else {
           $searchnm = '';
        }
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'users'
                ));
        $select->columns(array(  
         'username',
         'userRoleType',
         'uts',
         'isAcceptDesclaimer'=>new Expression("CASE WHEN t1.isAcceptDesclaimer= 0 THEN 'No' 
                                                 WHEN t1.isAcceptDesclaimer = 1 THEN 'Yes' 
                                           END"),   
        ));
        
        $select->join(
        array('t2' =>'vcard_search'),
        new Expression('t2.username = t1.username'),
        array(
       'countryCode',
       'firstName',
       'lastName',
       'email',
       'phone',
       'city',
//     'country',
       'companyName',
       'designation',
       'parentEmail',
       'parentMobile',
        'school',
       'ClassOrGrade',
       'dobStringDate',
        'dob',
          'collegeName'  
       ),
        'LEFT'
    );
        
       $select->join(array(
            't3' => 'userRoleType'
         ), 't3.id = t1.userRoleType', array(
            'role'
         ), 'LEFT');  
       
        
        
        $select->join(array(
            't4' => 'collegeAdmissionCountries'
         ), 't4.id = t2.country', array(
            'country'
         ), 'LEFT');   
        
  
        
        
    if ($data['firstNameOrder']=="asc"){
            $select->order(array(
          't2.firstName ASC'
         ));
    } elseif ($data['firstNameOrder']=="desc") {
            $select->order(array(
          't2.firstName DESC'
       ));
    } else{
       $select->order(array(
          't1.id DESC'
       ));
   }
   
   if($data['filter']=="totalUsers"){
   $select->where(array("t1.active=1 and t1.status =0 and t1.adminType !=2  and t1.testAccount='".$testAccount."'  $searchnm "));
       
   }else if($data['filter']=="totalStudents"){
    $select->where(array("t1.active=1 and t1.status =0 and t1.adminType !=2 and t1.userRoleType=1  and t1.testAccount='".$testAccount."'  $searchnm "));
      
       
   }else if($data['filter']=="totalAdmissionOfficer"){
    $select->where(array("t1.active=1 and t1.status =0 and t1.adminType !=2 and t1.userRoleType=2  and t1.testAccount='".$testAccount."'  $searchnm "));
      
       
  }else if($data['filter']=="highSchoolCounsellor"){
    $select->where(array("t1.active=1 and t1.status =0 and t1.adminType !=2  and t1.userRoleType=3 and t1.testAccount='".$testAccount."'  $searchnm "));
      
       
         
  }else if($data['filter']=="organisation"){
    $select->where(array("t1.active=1 and t1.status =0 and t1.adminType !=2 and t1.userRoleType=4  and t1.testAccount='".$testAccount."'  $searchnm "));
       
   }else{
   $select->where(array("t1.active=1 and t1.status =0 and t1.adminType !=2  and t1.testAccount='".$testAccount."'  $searchnm "));
   }
   
// $smt = $sql->prepareStatementForSqlObject($select);
//echo $smt->getSql(); die;
   
   if($pageing){
         $dbAdapter = new DbSelect($select, $this->getAdapter());
         $paginator = new Paginator($dbAdapter);
         return $paginator;
   } else {
         $smt = $sql->prepareStatementForSqlObject($select);
         $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
         return $result;
    }
  }
   //method for counter    
  public function getCounterOfUsers($data,$testAccount){
  
       $sql = new Sql($this->adapter);
       $select = $sql->select(array(
                        't1' =>'users'
                            ));
       $select->columns(array(
           'count' => new \Zend\Db\Sql\Expression('COUNT(t1.username)'),
       )); 
       
       $select->join(array('t2' =>'vcard_search'), new Expression('t2.username = t1.username'), array('firstName'),'LEFT');
       
     
       if($data =='totalUsers'){
       $select->where(array("  t1.testAccount='".$testAccount."' and t1.active=1  and t1.status =0 and t1.adminType !=2 "));
     
       }elseif($data=='totalStudents'){
       $select->where(array("  t1.testAccount='".$testAccount."' and t1.active=1 and t1.adminType !=2  and t1.status =0 and t1.userRoleType=1   "));
    
       }else if ($data=='totalAdmissionOfficer'){
       $select->where(array("  t1.testAccount='".$testAccount."' and t1.active=1 and t1.adminType !=2  and t1.status =0 and t1.userRoleType=2  "));
    
       }else if ($data=='highSchoolCounsellor'){
       $select->where(array("  t1.testAccount='".$testAccount."' and t1.active=1 and t1.adminType !=2 and t1.status =0 and t1.userRoleType=3  "));
     
       }else if ($data=='organisation'){
       $select->where(array("  t1.testAccount='".$testAccount."' and t1.active=1 and t1.adminType !=2  and t1.status =0 and t1.userRoleType=4  "));
      }
      
      $smt = $sql->prepareStatementForSqlObject($select);
//      echo $smt->getSql(); die;
      $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
      return $result[0]['count']; 
  }
  
public function getStudentInterest($userId){
    $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'studentSelectedInterest' 
          ));
          $select->columns(array(
           'id'=>'studentInterestId',
          ));
          $select->join(array(
            't2' => 'studentInterest'
         ), 't2.id = t1.studentInterestId', array(
                'name'
         ), 'LEFT');
    $select->where(array("t1.userId = '".$userId."' and t1.status=1"));
    $select->order('t2.name ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
    $media = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    $str = "";
    foreach($media as $skills){
       $str.=$skills['name'].",  "; 
    }
    return trim($str," ,");
  }
  
  
public function getInterestedCountries($userId){
    $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'userInterestedCountries' 
          ));
          $select->columns(array(
           'id'=>'countriesInterestedInId',
          ));
          $select->join(array(
            't2' => 'countriesInterestedIn'
         ), 't2.id = t1.countriesInterestedInId', array(
           'name'=>'country'
         ), 'LEFT');
    $select->where(array("t1.userId = '".$userId."' and t1.status=1"));
    $select->order('t2.country ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
    $media = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    $str = "";
    foreach($media as $skills){
       $str.=$skills['name'].",  "; 
    }
    return trim($str," ,");
  }
  
  
 public function majorsInterestedIn($userId){
    $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'userMajorsInterestedIn' 
          ));
          $select->columns(array(
           'id'=>'majorsInterestedInId',
          ));
          $select->join(array(
            't2' => 'majorsInterestedIn'
         ), 't2.id = t1.majorsInterestedInId', array(
           'name'=>'major'
         ), 'LEFT');
    $select->where(array("t1.userId = '".$userId."' and t1.status=1"));
    $select->order('t2.major ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
    $media = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    $str = "";
    foreach($media as $skills){
       $str.=$skills['name'].",  "; 
    }
    return trim($str," ,");
  }
  
  
   public function areaOfService($userId){
    $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'userAreaOfService' 
          ));
          $select->columns(array(
           'id'=>'areaOfServiceId',
          ));
          $select->join(array(
            't2' => 'areaOfinterest'
         ), 't2.id = t1.areaOfServiceId', array(
           'name'
         ), 'LEFT');
    $select->where(array("t1.userId = '".$userId."' and t1.status=1"));
    $select->order('t2.name ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
    $media = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    $str = "";
    foreach($media as $skills){
       $str.=$skills['name'].",  "; 
    }
    return trim($str," ,");
  }
  
  
  
   public function getPopularMajors($userId){
    $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'userPopularMajors' 
          ));
          $select->columns(array(
           'id'=>'popularMajorsId',
          ));
          $select->join(array(
            't2' => 'popularMajors'
         ), 't2.id = t1.popularMajorsId', array(
           'name'
         ), 'LEFT');
    $select->where(array("t1.userId = '".$userId."' and t1.status=1"));
    $select->order('t2.name ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
    $media = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    $str = "";
    foreach($media as $skills){
       $str.=$skills['name'].",  "; 
    }
    return trim($str," ,");
  }
  
  
  
  
  
  
  
  
     public function getOrganisationInterestedMeeting($userId){
    $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'organisationUserMeeting' 
          ));
          $select->columns(array(
           'id'=>'meetingId',
          ));
          $select->join(array(
            't2' => 'organisationMeeting'
         ), 't2.id = t1.meetingId', array(
           'name'
         ), 'LEFT');
    $select->where(array("t1.userId = '".$userId."' and t1.status=1"));
    $select->order('t2.name ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
    $media = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    $str = "";
    foreach($media as $skills){
       $str.=$skills['name'].",  "; 
    }
    return trim($str," ,");
  }
  
  
  
  public function getInterestedMeeting($userId){
    $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'userMeetingInterests' 
          ));
          $select->columns(array(
           'id'=>'meetingId',
          ));
          $select->join(array(
            't2' => 'meetingInterest'
         ), 't2.id = t1.meetingId', array(
           'name'=>'meeting'
         ), 'LEFT');
    $select->where(array("t1.userId = '".$userId."' and t1.status=1"));
    $select->order('t2.meeting ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
    $media = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    $str = "";
    foreach($media as $skills){
       $str.=$skills['name'].",  "; 
    }
    return trim($str," ,");
  }
  
 
  
    
}