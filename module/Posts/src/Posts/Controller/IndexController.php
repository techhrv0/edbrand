<?php
namespace Posts\Controller;
use Zend\View\Model\ViewModel;
use CustomLib\Controller\CustomController;
use Zend\Filter\StripTags;
use Zend\Filter\StringTrim;
use Zend\View\Model\JsonModel;
use CustomLib\Service\UserPassword;
use CustomLib\Service\ClickMeetingRestClient;

class IndexController extends CustomController
{
//method for root    
    public function indexAction()
    {
        return new ViewModel(array());
    } 
//method for gege guest    
public function manageUserAction(){
  $loginDetail = $this->getLoggedInUserId();
  $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable'); 
  $getUserCounter= $this->getUserCounter($loginDetail['testAccount']);
  
  
    //code for import data
//   $msg = "";
//   $request = $this->getRequest();
//    if ($request->isPost()) {
//     //$data = $request->getPost()->toArray();
////   if($data['Import']=="Import"){
//       $files = $request->getFiles()->toArray();
//     if(!empty($files['file']['name'])){
//           $allowedExtensions = array("xls","xlsx");
//           $ext = pathinfo($files['file']['name'], PATHINFO_EXTENSION);
//         if(in_array($ext, $allowedExtensions)) {
//               $file_size = $files['file']['size'] / 1024;
//              if($file_size < 1000) {   
//                $path="public/uploadedFiles/OstrichMedia/";
//                $file = $path.$files['file']['name'];
//                $isUploaded = copy($files['file']['tmp_name'], $file); 
//           if($isUploaded) {
//              try {
//                  $inputFileType = \PHPExcel_IOFactory::identify($file);
//                  $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
//                  $objPHPExcel = $objReader->load($file);
//              } catch(Exception $e) {
//                 die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
//              }            
//            $sheet = $objPHPExcel->getActiveSheet();
//            $sData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
//            $sheetData = array_slice($sData,0);
//       //  Loop through each row of the worksheet in turn
//     foreach($sheetData as $row){
////                     echo "<pre>"; print_r($row); die;
//
//      if(trim($row['A']) != "CountryName" && trim($row['A']) != "" ){
//          //check user exist or not
//          if(!empty(trim($row['A']))){
//               $val['country'] = trim($row['A']);
//             }else{
//               $val['country'] = "";  
//             } 
//            $val['testAccount'] = "3";
//            $val['createdOn'] =time(); 
//            $val['status'] ="1";
//            $val['updatedOn'] = time();   
//         $usrTlb->saveData('countriesInterestedIn', $val);
//          
//         }
//       } 
//       return $this->redirect()->toUrl('/eventprogram/manage-user'); 
//      }else{
//              $msg ="File not uploaded"   ;   
//           }     
//                        
//      }else{
//                      $msg ="Maximum file size should not cross 50 KB on size!";
//          }
//                  
//                  
//      } else{
//                   $msg ="This type of file is not allowed!";
//           }
//              
//     }else{
//              $msg ="Please select a file first"  ;
//     }        
//  }
  
  
 
   $view = new ViewModel(array( 'userCounter'=>$getUserCounter ));
   return $view;
 } 
 
 
 // for all users
 public function manageUserDataAction(){
  $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable'); 
  $request = $this->getRequest();
  $data = $request->getPost()->toArray();
  $loginDetail = $this->getLoggedInUserId();
//  echo '<pre>'; print_r($data); die;
  $paginator = $usrTlb->getAllUsers($data,true,$loginDetail['testAccount']);
  $results = $this->paginationToArray($paginator,$data['page'],10);
//  echo '<pre>'; print_r($results); die;
  

  foreach($results['allData'] as $key=>$value){
      $results['allData'][$key]['date'] = date("d-m-Y",$value['uts']);
      
      $results['allData'][$key]['countriesInterestedIn'] = $usrTlb->getInterestedCountries($value['username']);
      $results['allData'][$key]['majorsInterestedIn'] = $usrTlb->majorsInterestedIn($value['username']);
      $results['allData'][$key]['areaOfService'] = $usrTlb->areaOfService($value['username']);
      $results['allData'][$key]['popularMajors'] = $usrTlb->getPopularMajors($value['username']);
      $results['allData'][$key]['OrganisationInterestedInMeeting'] = $usrTlb->getOrganisationInterestedMeeting($value['username']);
      $results['allData'][$key]['interestedInMeeting'] = $usrTlb->getInterestedMeeting($value['username']);
      $results['allData'][$key]['studentsInterest'] = $usrTlb->getStudentInterest($value['username']);
      
  }
  
//  echo '<pre>'; print_r($results); die;
  
  $uri = $this->getRequest()->getUri();
  $mediaUrl = $this->getMediaUrl();
  $view = new ViewModel(array(
          'paginator' => $paginator,
          'uri' => $uri,
          'results' => $results,
          'mediaUrl'=>$mediaUrl
     ));
      $view->setTerminal(true);
      return $view;
} 
 
 
 private function timzeconvertion($datetime){
//$gmdate  = gmdate("Y-m-d H:i", strtotime($datetime));
//date_default_timezone_set('UTC');
//return strtotime($gmdate);
//echo $datetime; echo "<pre>"; print_r($gmdate); echo "<pre>"; print_r(strtotime($gmdate)); die;
date_default_timezone_set('Asia/Calcutta');
//$datetime = "2016-05-05 18:33:00";
$asia_timestamp = strtotime($datetime);
//echo date_default_timezone_get()."<br>"; // Asia/Calcutta
date_default_timezone_set('UTC');
//echo date_default_timezone_get()."<br>"; //UTC
$utcDateTime = date("Y-m-d H:i:s", $asia_timestamp);
return strtotime($utcDateTime);
//echo "<pre>";echo strtotime($utcDateTime);
//die;
}
 
//get counters For guest acc to dates:- 
 public function getUserCounter($testAccount){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\EventTable'); 
    $counters['totalUsers']    = $usrTlb->getCounterOfUsers('totalUsers',$testAccount);  
    $counters['totalStudents']   = $usrTlb->getCounterOfUsers('totalStudents',$testAccount);
    $counters['totalAdmissionOfficer']   = $usrTlb->getCounterOfUsers('totalAdmissionOfficer',$testAccount);
    $counters['highSchoolCounsellor']   = $usrTlb->getCounterOfUsers('highSchoolCounsellor',$testAccount);
    $counters['organisation']   = $usrTlb->getCounterOfUsers('organisation',$testAccount);
//    echo '<pre>';    print_r($counters); die;
    return $counters;
 } 
 
 
 



 

 
 
 
}