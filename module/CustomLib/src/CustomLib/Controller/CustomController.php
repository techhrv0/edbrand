<?php
namespace CustomLib\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\Validator\File\Extension;
use Zend\Validator\File\Size;
//use Zend\Validator\File\IsImage;
use Zend\InputFilter\FileInput;
use CustomLib\Service\S3;
use CustomLib\Service\ApiMessages;
//use CustomLib\Service\ImageManipulator;
use mails\PHPMailer;
use Zend\View\Model\ViewModel;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;

class CustomController extends AbstractActionController
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        // $this->_error = new customError();
        // $this->_utility = new customUtility();
    }

    /*
     * Get error instance
     *
     * @access public
     * @return \CustomLib\Service\customError
     */
    public function getErrorInstance()
    {
        return $this->_error;
    }

    /**
     * Get utility instance
     *
     * @access public
     * @return \CustomLib\Service\customUtility
     */
    public function getUtilityInstance()
    {
        return $this->_utility;
    }

    /**
     * Translate string
     *
     * @access protected
     * @param string $msg
     *            // message string
     * @return string
     */
    protected function translate($msg)
    {
        $translate = $this->getServiceLocator()
            ->get('viewhelpermanager')
            ->get('translate');
        return $translate($msg);
    }

    /**
     * Function to find action for module.
     *
     * @return String.
     */
    public function submenu()
    {
        return $this->_module;
    }
    /**
     * Get logged in user Id
     *
     * @access public
     * @return integer
     */
    public function getLoggedInUserId()
    {
        $session = new Container('User'); 
        $loginUserId['userId'] = $session->offsetGet('userId');
        $loginUserId['timeZone'] = $session->offsetGet('timeZone');
        $loginUserId['userTypeId'] = $session->offsetGet('userTypeId');
         $loginUserId['appName'] = $session->offsetGet('appName');
         $loginUserId['testAccount'] = $session->offsetGet('testAccount');
         $loginUserId['user_type'] = $session->offsetGet('user_type');
         $loginUserId['username'] = $session->offsetGet('username');
         
        return $loginUserId;
   }
  /**
     * Function to check user is login or not
     */
    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        //
        $module = $this->findModule($this->params('controller'), $this->params('action'));
        $userDetail = $this->getLoggedInUserId();
        //addRandomUser
        if (empty($userDetail['userId'])) {
            if (($this->params('action') != 'login') && ($this->params('action') != 'presentation') && ($this->params('action') != 'photo-grid') && ($this->params('action') != 'add-random-user') && ($this->params('action') != 'session-question') && ($this->params('action') != 'usersessionfeedback') && ($this->params('action') != 'thank-you')  && ($this->params('action') != 'quickPoll') && ($this->params('action') != 'app-feedback')   && ($this->params('action') != 'user-engagement-details') && ($this->params('action') != 'userEngagement') && ($this->params('action') != 'quickpollgraph')  && ($this->params('action') != 'quickpolltimeout')  && ($this->params('action') != 'timeout')  && ($this->params('action') != 'noquickpoll') && ($this->params('action') !='leaderboard') && ($this->params('action') !='sendnotificationpush') && ($this->params('action') !='sendemailque')){

                $this->redirect()->toUrl("/");
            }
        } else {
             $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
  
            $getUserName=$usrTlb->getUserName($userDetail['username']);
//        echo '<pre>'; print_r($getUserName); die;
            
            $viewModel = $e->getViewModel();
            $viewModel->moduleName = $module;
            $viewModel->actionName = $this->params('action');
            $viewModel->moduleId = $this->params('id');
            $viewModel->appMvc =    $this;
            $viewModel->mediaUrl = $this->getMediaUrl();
            $viewModel->appName = $userDetail['appName'];
            $viewModel->testAccount = $userDetail['testAccount'];
            $viewModel->name = $getUserName[0]['name'];
            
            if ($this->params('action') == 'login') {
                 $this->redirect()->toUrl('/eventprogram/manage-user');
            }
        }
        return parent::onDispatch($e);
    }
    /**
     * Function to find module and set the head title.
     *
     * @param String $controller            
     * @param String $action            
     */
    public function findModule($controller, $action)
    {
        $controllerParam = explode('\\', $controller);
        $viewHelperManager = $this->getServiceLocator()->get('viewHelperManager');
        $headTitleHelper = $viewHelperManager->get('headTitle');
        $headTitleHelper->append($controllerParam[0]);
        return $controllerParam[0];
    }

    /**
     *
     * @param unknown $paginator            
     * @param string $page            
     * @param string $counter            
     * @return multitype:string Ambigous <number, string> number unknown
     */
    protected function paginationToArray($paginator, $page = '', $counter = '')
    {
        $paginator->setCurrentPageNumber((int) $page);
        $paginator->setItemCountPerPage($counter);
        $totalCount = $paginator->getTotalItemCount();
        $lastPage = ceil($totalCount / $counter);
        $fullArray = \Zend\Stdlib\ArrayUtils::iteratorToArray($paginator); // $paginator->getIterator()->toArray();
        $nextPage = 0;
        if ($page == $lastPage || $lastPage == 0) {
            $nextPage = $page;
        } else {
            $nextPage = $page + 1;
        }
        return array(
            'page' => $page,
            'nextPage' => $nextPage,
            'allData' => $fullArray,
            'lastRecord' => $lastPage,
            'pageCount' => $counter,
            'totalCount' => $totalCount
        );
    }
//method for s3 bucket upload    
    public function putIntoS3Bucket($filePath = '', $fileName = '',$type=1) {
		// Bucket settings.
		$config = $this->getServiceLocator ()->get ( 'config' );
		$bucketSetting = $config ['S3BUCKET'];
//                echo '<pre>'; print_r($bucketSetting); die(' bbn');
		$AWSkey = $bucketSetting ['AWSKEY'];
		$AWSSecret = $bucketSetting ['AWSSECRET'];
		$s3 = new S3 ( $AWSkey, $AWSSecret );
		// create bucket for all files.
		$bucketName = $bucketSetting ['FILEBUCKET'];
		$s3->putBucket ( $bucketName, S3::ACL_PUBLIC_READ );
                if($type == 2){
                  $contentType = 'video/mp4';
               }else{
                $contentType = '';
               }
		// move the file
		if ($s3->putObjectFile ( $filePath, $bucketName, $fileName, S3::ACL_PUBLIC_READ,array(),$contentType)) {
			return "success";
		} else {
			return "failure";
		}
	}
        //
//method for getting user time
//time type 1 for d M Y H:i:s, 2 for d M Y format
 public function getUserTimeZone($utc,$usertimezone,$timeType){ 
    if(empty($utc) || empty($usertimezone)){
        return "";
    } 
    $dt = new \DateTime($utc);
    $tz = new \DateTimeZone($usertimezone);
    $dt->setTimezone($tz);
    if($timeType==1){
     $time = $dt->format('d M Y H:i:s');
    } if($timeType==3){ 
     $time = $dt->format('d-m-Y H:i');
    }else {
     $time = $dt->format('d-m-Y');   
//     $time = $dt->format('d M Y');   
    }
    
    return $time; 
 } 
 //method for getting user time
//time type 1 for d M Y H:i:s, 2 for d M Y format
 public function getUserTimeZonerTime($utc,$usertimezone,$timeType){ 
    if(empty($utc) || empty($usertimezone)){
        return "";
    } 
    $dt = new \DateTime($utc);
    $tz = new \DateTimeZone($usertimezone);
    $dt->setTimezone($tz);
    if($timeType==1){
     $time = $dt->format('H:i');
    }else {
     $time = $dt->format('d M Y');   
    }
    
    return $time; 
 } 
public function filterinput($data){
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;  
}
 public function getUserTimeZoneCustom($utc,$usertimezone){
    if(empty($utc) || empty($usertimezone)){
        return "";
    } 
    $dt = new \DateTime($utc);
    $tz = new \DateTimeZone($usertimezone);
    $dt->setTimezone($tz);
    $dateTime['time'] = $dt->format('H:i:s');
    $dateTime['date'] = $dt->format('Y-m-d');
    return $dateTime; 
 }    
 public function sendMail($data) {
        $phpmailer = new PHPMailer();

        $phpmailer->IsSMTP();

        $phpmailer->Host = "mail.approutes.com";

        $phpmailer->SMTPAuth = true;

        $phpmailer->SMTPSecure = "ssl";

        $phpmailer->Username = 'rsvp@approutes.com';

        $phpmailer->Password = 'RSVP@app@2013';

        $phpmailer->Port="465";

        $phpmailer->From= $data['From'];

        $phpmailer->FromName= $data['mailFromNickName'];
        //$phpmailer->Sender="";
        $phpmailer->AddAddress($data["mailTo"]);
        //$phpmailer->AddBCC("bhupendra@approutes.com","bhupi");
        $phpmailer->Subject = $data["mailSubject"];
        $phpmailer->IsHTML(TRUE);
        if(isset($data["path"]))
        {
            //$phpmailer->AddAttachment($data["path"], $data['fullName'],  'base64', 'application/pdf');
            $phpmailer->AddEmbeddedImage($data["path"], $data['imageName'], $data['imageName']);
        }
        $phpmailer->Body = $data["mailBody"];
        if(!$phpmailer->Send())
        {
           $error = "Error sending: " . $phpmailer->ErrorInfo;
        }
        else
        {
           $error = "success";
        }
        //echo $error; die;
        return $error;
 }
 public function insertJobQueue($data)
    {
        $config = $this->getServiceLocator()->get('config');
        \Resque::setBackend('localhost:6379');
        $args = array(
            'name' => 'Special-Job',
             'redisHost' => 'localhost:6379',
        );
        if(empty($data)) {
            $args = array_merge($args , array(
                'url' => $config['settings']['JobQueUrl'].'/jobque/sendticketmail',
                'jobClass' => 'JobQueue', //name of the Job Class
                'queue' => 'SpecialJobQ',  //name of the queue
                'time' => time()
            ));
        }else{
            $args = array_merge($args,$data);
        }
        
        $jobId = \Resque::enqueue($args['queue'], $args['jobClass'], $args);
        
        return $jobId;
    }
public function getUserId($length) {
        $token = "";
        $codeAlphabet= "abcdklmvwxnopeiifghijqrstuyz";
        $codeAlphabet.= "0142376895";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
    public function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }
  //get salt
  public function getSalt($length) {
     $token = "";
     $codeAlphabet="abcdklmvwxnopeiifghijqrstuyz";
     $codeAlphabet.="0142376895re5347015ff";
     $max = strlen($codeAlphabet) - 1;
     for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
     }
    return $token;
 }
 public function getMediaUrl(){
    $config = $this->getServiceLocator()->get('config');
    $bucketSetting = $config['S3BUCKET'];
    $bucketUrl = $bucketSetting['BUCKETURL'];
    return $bucketUrl;
 } 
 public function getExtension($fileType,$ext){
    $config = $this->getServiceLocator()->get('config');
    $bucketSetting = $config['settings'][$fileType];
    if(in_array($ext, $bucketSetting)){
            return "success";
     } else {
         return "failure";
     }        
 } 
 public function sendMails($msg,$flag=0){
      $view = $this->getServiceLocator()->get('ViewRenderer');
      $viewModel = new ViewModel();
      $viewModel->setVariables(array('msg'=>$msg,'flag'=>$flag));
      $viewModel->setTemplate('application/index/send-mail.phtml');
      $content = $view->render($viewModel);
      return $content;
  }
  public function vaildateFile($file,$type=1){
     $fileFilter = new FileInput();
     if($type==1){
        $fileFilter->getValidatorChain()->attach(
            new Extension(array('jpg', 'jpeg',
           'png','JPG', 'JPEG', 'PNG')));
       $fileFilter->getValidatorChain()->attach(
            new Size(array('min' => '1kB', 'max' => '1MB'))); 
     }else{
         $fileFilter->getValidatorChain()->attach(
            new Extension(array('3gp', '3gpp', 'mp4', 'flv', '3GP', '3GPP', 'MP4', 'FLV',
            'mpeg', 'mpeg4', 'mkv', 'dat', 'avi', 'mov',
            'MPEG', 'MPEG4', 'MKV', 'DAT', 'AVI', 'MOV',
            'wmv', 'mpg', 'WMV', 'MPG')));
      $fileFilter->getValidatorChain()->attach(
            new Size(array('min' => '1kB', 'max' => '10MB')));
     }
     $fileFilter->setValue($file);
     if ($fileFilter->isValid()) {
            return "success";
     } else {
        return $fileFilter->getMessages();
    }
 }
  public function newsendvoippush($payload = array(),$message = '', $deviceId = 0, $type = 'dev',$appType){
        //first check deviceId empty or not
         if(empty($deviceId)){
            return "success";
         }
     //now code for send push using curl method    
       if(defined('CURL_HTTP_VERSION_2_0')){
         $device_token   = $deviceId;       
         $config = $this->getServiceLocator()->get('config');
         if($appType=="devilsCircuit"){
           $pem_file  = $config['settings']['PUSH_FILED'];
          }elseif($appType=="techHr"){
           $pem_file  = $config['settings']['PUSH_FILET'];
          }elseif($appType=="knot"){
           $pem_file  = $config['settings']['PUSH_FILEK'];
          }elseif($appType=="apekshah"){
           $pem_file  = $config['settings']['PUSH_FILEAp'];   
          }elseif($appType=="adobe"){
            $pem_file  = $config['settings']['PUSH_FILEAD'];     
          } 
         //$pem_secret     = 'your pem secret';
         $apns_expiration     = time() + 31536000;
         $apns_priorty = 10;
         $sample_alert = json_encode($payload);
        if($type=="dev"){
         $url = "https://api.development.push.apple.com/3/device/$device_token";
        }else{
          $url = "https://api.push.apple.com/3/device/$device_token";   
        }
        //
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sample_alert);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, 3);
        curl_setopt($ch, CURLOPT_PORT, 443);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER ,TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT ,30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER ,false);
        curl_setopt($ch, CURLOPT_HEADER ,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("apns-expiration: $apns_expiration","apns-priority: $apns_priorty"));
        curl_setopt($ch, CURLOPT_SSLCERT, $pem_file);
         //curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pem_secret);
         $response = curl_exec($ch);
         if ($response === FALSE) {
           throw new \Exception("Curl failed: " .  curl_error($ch));
         }
         $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);
         return $httpcode;
      }
     }
 public function sendAndroidPush($pushData,$appType) {
    if($pushData["deviceId"]!= "") {        
       $notificaationCount = 0;   
       if(isset($pushData["pushBadge"]) && $pushData["pushBadge"]!='') {
           $notificaationCount = $pushData["pushBadge"];
        }
        if($appType=="devilsCircuit"){
         $apiKey ="AAAAuSf_cD4:APA91bG5jqLE9voHpItd6oBAPbJb6uRe3Zu3zVm2kw7SPLdBDEg07mHxhxvptGF5ypdlW2xQO7GRhrqOgmxFYkfoT0CSzZg1Sy0aqDIBimxs8ZaeSk7cnKFao7SXw3A7gEQM8Y5NFip_";
        }elseif($appType=="ArYaReadyVas"){
         $apiKey ="AAAAbIscJpg:APA91bHfcHgRsPBV0ERC9pMHwDsFrbwo4oDNtqL5R2sb_MhemfMmuY_tD_M5DX1qahH_PUJzUAZ4XqGWzCK5lwDZwGjgDGz4HjMkBW4_00xPRai5mQWe4qNrddMpaJ_Od3UBrPHzeUKz"; 
        }elseif($appType=="knot"){
         $apiKey = "AAAAmWJ7jt8:APA91bFZEPrVKkAIpQvcakYk_iBndTkAU8BYstzTsX9B-2nZq0uThsrvzSDO3f97wrC0zEST6V76xo9LJIxbC2BO-g_z7G2sDId-Yf6J07dA3m9CLLxcH-tczSH7ganCd1F69YAzHZXW"; 
        }elseif($appType=="apekshah"){
         $apiKey = "AAAAM6WiAvU:APA91bFjXCeapO0Ecenah05NP_u6tDFSpj7OVh7BLw3e_R3NLWwZB72N0f-JJTO2-xpZ_cW0Wu7PdOi5BH3RxQIxLg5ecPqX2wq66pGBE4gkbEBzhvPwvl_z60tyR30sfMYV27jCO0DB";  
        }elseif($appType=="techHr"){
         $apiKey ="AAAAYU5jiRw:APA91bH-dppYPTRmaUvP7dB7nAAFu3DB7wNpQYq0d1yHj7iXbyesIctmiZnOgQaXiC_MaUfls_OeE4WA1txrGu3B9lIaK5ZgdMao_c6cY6CUZwXhu0lXf3i5B2AJ5z3TvkJ-L6wEAWUc";
        } 
        $registrationIDs = array($pushData["deviceId"]);
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
                'registration_ids' => $registrationIDs,
                'data' => array(
                    "message" => $pushData["message"],
                    'title'  => $pushData["subject"],
                    'pType'=>$pushData['pType'],
                    'subtitle'	=> '',
                    'tickerText'=>'',
                    'vibrate'=> 1,
                    'sound'=> 1,
                    'largeIcon'	=>'large_icon',
                    'smallIcon'	=>'small_icon',
                    "badge" => $notificaationCount
                    ),
                'delay_while_idle' => true,
                'priority'=>'high'
              );
        $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
           return 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
      }
    }
//get Token    
     public function getToken($length) {
        $token = "";
        $codeAlphabet = "IXQAZDWSELRFCVSPOMBGLKTYUJHG";
        $codeAlphabet.= "abcdklmvwxnopeiifghijqrstuyz";
        $codeAlphabet.= "0142376895";
        //$codeAlphabet.= "$&*@!%";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
 //method for create default pic   
  public function createDefaultUserPic($postData = array(), $config = array()) 
    {
            $sAppUsername = $postData["firstName"];
            $word = strtoupper(substr($sAppUsername,0,1));
            if(ctype_alpha($word)){
               $word = strtoupper(substr($sAppUsername,0,1));
            }else{
             $word = strtoupper(substr($sAppUsername,1,1));  
            }
            $defaultProfilePicsDir = $config['settings']['DEFAULT_PROFILE_PICS_LOCATION'];
            $defaultProfilePic = $config['settings']['POST_IMAGE_FILE_PATH'];
            $defaultProfilePicFont = $config['settings']['PROFILE_PIC_FONT_LOCATION']."helvetica.ttf";            
            $images = glob("$defaultProfilePicsDir{*.jpg,*.JPG,*.jpeg, *.JPEG}", GLOB_BRACE);
            $rndCount = rand(1,count($images));
            $img = $defaultProfilePicsDir.$rndCount.'.jpg';
            $im = imagecreatefromjpeg($img);
            list ($width, $height) = getimagesize($img);
            $textcolor = imagecolorallocate($im, 255, 255, 255);
            list($xVal,$yVal) = $this->findImageTTFCenter($im, $word, 2, $defaultProfilePicFont, 300);
            imagettftext($im, 300, 0, $xVal, $yVal, $textcolor, $defaultProfilePicFont, $word);
            $sTempName = explode ( '.', $img );
            $newFileName = uniqid() . "." . end ( $sTempName );
            imagejpeg($im,$defaultProfilePic.$newFileName);
            shell_exec("chmod -R 0777 ".$defaultProfilePic.$newFileName);
            $status = $this->putIntoS3Bucket($defaultProfilePic.$newFileName,"profile/".$postData['userId'].".png");
            imagedestroy($im);
            $defaultPic = $newFileName;
            unlink($defaultProfilePic.$newFileName);
            return $defaultPic;
      }
        
        public function findImageTTFCenter($image, $text,$angle ,$font, $size) 
        {
        	// find the size of the image
        	$xi = ImageSX($image);
        	$yi = ImageSY($image);
        
        	// find the size of the text
        	$box = ImageTTFBBox($size, $angle, $font, $text);
                
        
        	$xr = abs(max($box[2], $box[4]));
        	$yr = abs(max($box[5], $box[7]));
        
        	// compute centering
        	$x = intval(($xi - $xr - 0) / 2);
        	$y = intval(($yi + $yr) / 2);
        	return array($x, $y);
        
       }  
 public function getTicket($length) {
        $token = "";
        $codeAlphabet = "IXQAZDWSELRFCVSPOMBGLKTYUJHG";
        $codeAlphabet.= "0142376895";
        //$codeAlphabet.= "$&*@!%";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
//method for get video frame(new method)    
 public function getVideoFrame($vedioFilePath = '',$vedioName =''){
   try
    	{
	    	$sourcePath = $vedioFilePath.$vedioName;
	    	$destinationPath = time (). ceil((rand(0,10000))).'.png';
	    	$frameTime  = '0.01';
                $thumbnail = $vedioFilePath.$destinationPath;
               
	    	$command = 'ffmpeg -i "'.$sourcePath.'" -ss '.$frameTime.' -vframes 1 "'.$thumbnail.'"';	
//	    	$command = "ffmpeg -v 0 -y -i $sourcePath -vframes 1 -ss 10 -vcodec mjpeg -f rawvideo -s 210x140 -aspect 16:9 $thumbnail";
                system($command);
                
	    	return $destinationPath;
	    	
    	} catch (\Exception $e){
    		return $e->getMessage();
        }
 }
//method for resize of image for post     
   public function reSizeImageforpost($filePath = '', $fileName = '', $w = 320, $h = 568, $compress = 0)
    {
        $file = $filePath . $fileName;
        list ($width, $height) = getimagesize($file);
        $r = $width / $height;        
        $src = $this->imagecreatefromfile($file);
        $dst = imagecreatetruecolor($width, $height);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $height);
        $destinationPath = sha1(time() . ceil((rand(0, 10000)))) . '.jpg';
        if ($compress) {
            imagejpeg($dst, $filePath . $destinationPath, $compress);
        } else {
            imagejpeg($dst, $filePath . $destinationPath);
        }
        return $destinationPath;
   }    
  function imagecreatefromfile($filename)
    {
        switch (strtolower(pathinfo($filename, PATHINFO_EXTENSION))) {
            case 'jpeg':
            case 'jpg':
                return imagecreatefromjpeg($filename);
                break;
            
            case 'png':
                return imagecreatefrompng($filename);
                break;
            
            case 'gif':
                return imagecreatefromgif($filename);
                break;
            
            default:
                return 'File "' . $filename . '" is not valid jpg, png or gif image.';
                break;
        }
    } 
//send xmmpp push
public function sendXmPush($data){ 
        $data_string = \GuzzleHttp\json_encode($data);
        $url = ApiMessages::chatUrl.':5280/notify';
        $ch = curl_init($url);                                                                                                                                        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')                                                                        
        );                                                                                                                   
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        return $status;
} 
//method for generate qr code
 public function saveQrCode($qrValue,$qrName){
   // Create a basic QR code
   $qrCode = new QrCode($qrValue);
   $qrCode->setSize(250);
   // Set advanced options
   $qrCode->setWriterByName('png');
   $qrCode->setMargin(10);
   $qrCode->setEncoding('UTF-8');
   $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH);
   $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
   $qrCode->setBackgroundColor(['r' => 238, 'g' => 236, 'b' => 224, 'a' => 1]);
   $qrCode->setLabel('', 16,'public/assets/noto_sans.otf', LabelAlignment::CENTER);
   $qrCode->setValidateResult(false);
    // Directly output the QR code
    // Save it to a file
   $path = 'public/uploadedFiles/Invite/'.$qrName.'.png';
   $qrCode->writeFile($path);
    //save image in s3 Bucket
   $status = $this->putIntoS3Bucket($path, "qrCode/".$qrName.".png");
  if ($status){
      unlink($path);
   }
   else {
     unlink($path);
   }
   return 'success';
 }
 
 
 
  //method for send apns push to ios client
 public function sendapnspush($payload = array(),$message = '',$deviceId = 0,$type = 'dev',$appType,$alertType){
    //first check deviceId empty or not
      if(empty($deviceId)){
         return "success";
      }
     //now code for send push using curl method    
      if(defined('CURL_HTTP_VERSION_2_0')){
         $device_token   = $deviceId;       
         $config = $this->getServiceLocator()->get('config');
         if($appType=="devilsCircuit"){
           $pem_file  = $config['settings']['PUSH_FILEDAPNS'];
           $app_bundle_id = "com.homemade.chef.prod";
         }elseif($appType=="ArYaReadyVas"){
           $pem_file  = $config['settings']['PUSH_FILETAPNS'];
           $app_bundle_id = "com.aryajain";
         }elseif($appType=="knot"){
           $pem_file  = $config['settings']['PUSH_FILEDAPNS'];
           $app_bundle_id = "com.approutes.demo";
         }elseif($appType=="techHrAdmin"){
           $pem_file  = $config['settings']['PUSH_FILEAdAPNS'];
           $app_bundle_id = "com.homemade.chef.prod";
         }elseif($appType=="apekshah"){
           $pem_file  = $config['settings']['PUSH_FILEApAPNS'];
           $app_bundle_id = "com.homemade.chef.prod";
         }elseif($appType=="adobe"){
           $pem_file  = $config['settings']['PUSH_FILEADAPNS'];
           $app_bundle_id = "com.adobefit.qa";
         }elseif($appType=="techHr"){
           $pem_file  = $config['settings']['PUSH_FILEDAPNS'];
           $app_bundle_id = "com.approutes.demo";
         }
         $apns_expiration     = time() + 31536000;
         $apns_priorty = 10;
         $sample_alert = json_encode($payload);
        if($type=="dev"){
         $url = "https://api.development.push.apple.com/3/device/$device_token";
        }else{
         $url = "https://api.push.apple.com/3/device/$device_token";   
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sample_alert);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, 3);
        curl_setopt($ch, CURLOPT_PORT, 443);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER ,TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT ,30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER ,false);
        curl_setopt($ch, CURLOPT_HEADER ,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("apns-push-type: {$alertType}"));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("apns-expiration: $apns_expiration","apns-priority: $apns_priorty","apns-topic: {$app_bundle_id}"));
        curl_setopt($ch, CURLOPT_SSLCERT, $pem_file);
        $response = curl_exec($ch);
        if ($response === FALSE) {
           throw new \Exception("Curl failed: " .  curl_error($ch));
        }
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $response;
      }
 }
}