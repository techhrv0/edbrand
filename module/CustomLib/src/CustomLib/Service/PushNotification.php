<?php

namespace CustomLib\Service;

class PushNotification {

    public function sendPush($pushData = array()) {
         
//        $apnsHost = 'gateway.push.apple.com';
        $pushType = '';
        $notificaationCount = 0;
        $notificaationCountOther = 0;
        
        if(isset($pushData["pushBadge"]) && $pushData["pushBadge"]!='') {
            $notificaationCount = $pushData["pushBadge"];
        }
        
        if(isset($pushData["pushType"]) && $pushData["pushType"]!='') {
            $pushType = $pushData["pushType"];
        }
        
        if(isset($pushData["deviceType"]) && $pushData["deviceType"]==2) {
            
            $this->sendAndroidPush($pushData);
            
        } else {
        
    //        $apnsHost = 'gateway.push.apple.com';
            $apnsHost = 'gateway.sandbox.push.apple.com';
            $apnsPort = '2195';
            $apnsCert = $_SERVER["DOCUMENT_ROOT"].'/staging.pem';
            $passPhrase = '';
            $streamContext = stream_context_create();
            stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
            $apnsConnection = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
            if ($apnsConnection == false) {
                echo "Failed to connect {$error} {$errorString}\n";
                return;
            } else {
                //echo "Connection successful";	
            }
            $message = $pushData["message"];
    //        $payload['aps'] = array('alert' => $message, 'sound' => 'default');
    //        $payload['aps'] = array('alert' => $message, 'sound' => 'default','pType' => $pushType);
            $payload['aps'] = array('alert' => $message, 'sound' => 'default','pType' => $pushType, 
                        'badge' => $notificaationCount,
                        'content-available' => 1
                        );
    //        $payload['stype'] = array('stype' => $pushData["pushType"]);
            $payload = json_encode($payload);
    //        $deviceToken = "647100f252e45be0863c2f1b91cd7e864b6ae92ab9015317c0a0e70fcc3cdfff";
            $deviceToken = $pushData["deviceId"];
            try {
                if ($message != "") {
                    $apnsMessage = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n", strlen($payload)) . $payload;
                    if (fwrite($apnsConnection, $apnsMessage)) {
                        //echo "true";
                    } else {
                        //echo "false";
                    }
                }
            } catch (Exception $e) {
                echo 'Caught exception: ' . $e->getMessage() . "\n";
            }
        }
    }
    
    public function sendAndroidPush($pushData = array()) {
        
        if ($pushData["deviceId"] != "") {
             $apiKey = 'AIzaSyBdQWNQRt2NhbsdE04WMRr3Q8LTcTW0T_4'; // not register
            //$apiKey = 'AIzaSyCAztgldMMkFDSjFfTpktXdCefYAAALeMg'; // unauthorize
            //$apiKey = 'AIzaSyAibbGAABE3cHq7XwutxOKGzTlO8jSfgKo'; // not register
             //$apiKey =  'AIzaSyAqVZ3vgu54YSs6pNFgh6-xcBMzlXa1wok'; //unauthorize
            $registrationIDs = array($pushData["deviceId"]);
            $msg = array
        (
            'message' 	=> $pushData["message"],
            'title'     => $pushData["subject"],
            'serverUrl' => $pushData["serverUrl"],   
            'newsData'=>$pushData['newsData'],   
            'subtitle'	=> '',
            'pType'=>$pushData["pType"],
            'tickerText'	=> '',
            'vibrate'	=> 1,
            'sound'		=> 1,
            'largeIcon'	=> 'large_icon',
            'smallIcon'	=> 'small_icon',
        );

            $url = 'https://android.googleapis.com/gcm/send';
            $fields = array(
                'registration_ids' => $registrationIDs,
                 'data'			=> $msg,
                'delay_while_idle' => true
            );
            $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);
            
            //print_r($result); die;
            if (curl_errno($ch)) {
                echo 'Curl error: ' . curl_error($ch);
            }
            curl_close($ch);
           //echo $result; die;
        }
    }

}
