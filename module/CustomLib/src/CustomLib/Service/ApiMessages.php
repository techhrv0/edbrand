<?php

namespace CustomLib\Service;

class ApiMessages {

    
    // COMMON STATUS
    const SUCCESS = 1;
    const FAILURE = 0;
    const SUCCESS_REQUEST = 200;
   const FAILURE_REQUEST = 400;
   const AUTH_FAILURE = 401;
   const SERVER_ERROR = 500;
    const RECORD_NOTFOUND = 404;
    // COMMON MESSAGES - START FROM HERE
    const AUTHENTICATION_VIOLATION = 'Error!! Authentication violation.';
    const INVALID_PARAMETERS = 'Invalid Parameters.';
    const ERROR_IN_YOUR_POST_REQUEST = 'Error! In your post';
    const ERROR_FOUND = 'Error Found';
    const INVALID_EMAIL = 'Invalid Email';
    // COMMON MESSAGES - ENDS FROM HERE


    const INFO_EN_WRONG_PASSWORD = 'The password you have entered is incorrect.';
    
    const INFO_EN_SUCCESS_LOGIN = 'Successfully Login.';
    const INFO_DE_SUCCESS_LOGIN = 'Successfully Login.';
    
    const INFO_EN_WRONG_USER_PASSWORD = 'Wrong username and/or password.';
    
    const INFO_EN_SUCCESS_LOGOUT = 'Logout Successfully.';
    
    const INFO_EN_FACEBOOK_ACCOUNT_NOT_EXISTS = 'Facebook account not exists.';
    
    const USER_ADDED_SUCCESSFULY = 'User added successfully.';
    
    const EMAIL_ALREADY_EXISTS = 'Email already exists.';
    
    const ERROR_EN_GLOBAL_AUTH_VIOLATION = 'Error!! Authentication violation.';
    
    const ERROR_EN_GLOBAL_MESSAGE = "Error Found !!!";
    // constant realeted to quickblox
    const qbAppId ="5";
    const qbAuthKey ="ZgXnFatnBSxZhpD";
    const qbAuthSecret ="ztLGb3BsTPkLV5t";
    const qbAccountKey ="gXMYy7E6Jyv2snRHHn2E";
    const fourSquareId ="YKSFTFURQKGZUFYEFGUDXHRLUEGTLEOXMBYUTU3AYA4LLKX5";
    const fourSquareSecret ="BH3IPIEM33UPG14QTDVCICC142Q3M34TN0EVT55NNBOZY4GX";
    //const for push notification
    const likePost = "like_post";
    const likeMediaPost = "like_media_post";
    const commentOnPost = "comment_on_post";
    const commentOnMedia = "comment_on_media";
    const resharePost = "reshare_post";
    const taggedFriend = "tagged_friend";
    const logout = "logout";
    const qbApiEndPoint = "https://apiciao.quickblox.com";
    const qbChatEndPoint  = "chatciao.quickblox.com";  
    const chatUrl = "https://chat.ostrich.co";
   }
